//
//  CloseCrossButtonCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import <ImoDynamicTableView/ImoDynamicDefaultCell.h>

@interface CloseCrossButtonCellSource : IDDCellSource

@property (nonatomic,retain) UIColor *backgroundColor;
@property (assign, nonatomic) BOOL showRestoreButton;

@end


@interface CloseCrossButtonCell : ImoDynamicDefaultCellExtended

@property (weak, nonatomic) IBOutlet UIButton *restoreButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (void)setUpWithSource:(CloseCrossButtonCellSource *)source;

@end

