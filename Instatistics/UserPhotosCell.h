//
//  UserPhotosCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "IGUser.h"

@interface UserPhotosCell: UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) IGUser *user;
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) UIRefreshControl *bottomRefreshControl;

@end

