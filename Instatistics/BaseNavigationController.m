//
//  BaseNavigationController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/7/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseNavigationController.h"
#import "AppUtils.h"


@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationBar.barTintColor = [AppUtils appBackgroundColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
