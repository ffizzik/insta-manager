//
//  UserStatisticsViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "UserStatisticsViewController.h"
#import "AppDelegate.h"
#import "CustomInputView.h"
#import "UserStatisticsTopMenuItem.h"
#import "UserStatisticsFollowInfoCell.h"
#import "LeftMenuView.h"
#import "UserEventsViewController.h"
#import "IGMedia.h"
#import "RegisteredUser.h"
#import "MainId.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "StalkersCell.h"

#import "UserImageTableViewCell.h"
#import "RefreshUploadingCounter.h"
#import "NotificationCenter.h"

#import "MWStalkersViewController.h"
#import "MWExtraViewController.h"
#import "MWSearchViewController.h"
#import "MWDeletedLikesViewController.h"
#import "MWUpgradeViewController.h"

#import "MWDataCollector.h"
#import "PDKeychainBindings.h"

typedef NS_ENUM(NSUInteger, TableViewState) {
    FullTableViewState = 0,
    PartTableViewState
};

@interface UserStatisticsViewController ()

@property (weak, nonatomic) IBOutlet UIButton *leftMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@property (nonatomic, retain) NSString *followersMaxId;

@property (assign, nonatomic) NSInteger requests;
@property (assign, nonatomic) NSInteger dbRequests;

@property (assign, nonatomic) CGFloat refreshPercents;
@property (assign, nonatomic) BOOL isRefreshing;
 
@property (weak, nonatomic) IBOutlet UIImageView *userMainPhotoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userBackgroundImageView;

@property (strong, nonatomic) UIImageView *gradientView;

@property (strong, nonatomic) RefreshUploadingCounter *refreshUploadingCounter;

@property (assign, nonatomic) TableViewState tablewViewState;

@property (strong, nonatomic) NSMutableArray *getLikesResponseArray;
@property (strong, nonatomic) NSMutableArray *getFollowersResponseArray;
@property (strong, nonatomic) NSMutableArray *getFollowingsResponseArray;

@end

@implementation UserStatisticsViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _requests = 0;
    _dbRequests = 0;
    _refreshPercents = 0;

    _tablewViewState = FullTableViewState;
    _refreshUploadingCounter = [[RefreshUploadingCounter alloc] initWithDBRequestsCount:11 FetchRequestsCount:3];
    
    [self refreshUserData];
    [self configureProgressHUD];
    [self setTitleUpImage:YES];
    
    if (self.mainUser.photos.count) {
        IGMedia *media = self.mainUser.photos[0];
        if (media.image.standard_resolution)
            [_userBackgroundImageView sd_setImageWithURL:[NSURL URLWithString:media.image.standard_resolution]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = YES;
     _isVisible = YES;
    
    [_leftMenuButton setImageEdgeInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    [_searchButton setImageEdgeInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    
    if (!self.gradientView) {
        self.gradientView = [[UIImageView alloc] initWithFrame:self.userBackgroundImageView.frame];
        self.gradientView.image = [CoreImageUtils imageFromVerticalGradientStartColor:[UIColor clearColor] EndColor: UIColorFromRGBA(COLOR_DARK_PURPLE, 1) Frame:self.gradientView.bounds];
        self.gradientView.tag = 175;
        
        if (![self.gradientView isDescendantOfView:self.userBackgroundImageView])
            [self.userBackgroundImageView insertSubview:self.gradientView atIndex:0];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NotificationCenter sharedNotificationCenter] requestAuthorization];
    
    if ([[PDKeychainBindings sharedKeychainBindings] objectForKey:@"MWNewProfileStalkersNotificationState"]) {
        if ([[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"MWNewProfileStalkersNotificationState"] isEqualToString:@"off"]) {
            return;
        }
    }
    
    if (![NotificationCenter sharedNotificationCenter].notificationsInstalled) {
        [[NotificationCenter sharedNotificationCenter] setNotificationsWithTimeInterval:60.0 * 60.0 * 24 Repeat:true];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    _isVisible = NO;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.gradientView.frame = self.userBackgroundImageView.bounds;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Requests

- (void)getFollowersForId:(NSString *)nextId {
  
    MainUser *mainUser = [MainUser mainUser];

    [[InstagramAPI sharedInstance] getUserFollowers:mainUser.user.Id count:0 nextCursor:nextId comp:^(NSArray *response, IGPagination *pagination) {
        [self.getFollowersResponseArray addObjectsFromArray:response];
        
        if ([[AppUtils validateString:pagination.nextMaxId] isEqualToString:@""] == NO) {
            [self getFollowersForId:pagination.nextMaxId];
            
        } else {
            [self.refreshUploadingCounter setPercents:100 ForFetchRequestIndex:1];
            [self percentLabelRefresh];
            [self handleGetFollowersResponse];
        }
        
    } failure:^(NSError *error) {
        _requests = 0;
        [self stopRefreshing];
    }];
}

- (void)getFollowingsForId:(NSString *)nextId {
    
    MainUser *mainUser = [MainUser mainUser];

    [[InstagramAPI sharedInstance] getUserFollowing:mainUser.user.Id count:0 nextCursor:nextId comp:^(NSArray *response, IGPagination *pagination) {
        [self.getFollowingsResponseArray addObjectsFromArray:response];
        
        if ([[AppUtils validateString:pagination.nextMaxId] isEqualToString:@""] == NO) {
            [self getFollowingsForId:pagination.nextMaxId];
            
        } else {
            [self.refreshUploadingCounter setPercents:100 ForFetchRequestIndex:2];
            [self percentLabelRefresh];
            [self handleGetFollowingsResponse];
        }
        
    } failure:^(NSError *error) {
        _requests = 0;
        [self stopRefreshing];
    }];
}

- (void)getLikesForId:(NSString *)nextId {
    
    MainUser *mainUser = [MainUser mainUser];

    [[InstagramAPI sharedInstance] getUserFeed:mainUser.user.Id count:0 maxId:nextId getComments:YES comp:^(NSArray *response, IGPagination *pagination) {
        [self.getLikesResponseArray addObjectsFromArray:response];
        
        if ([[AppUtils validateString:pagination.nextMaxId] isEqualToString:@""] == NO) {
            [self getLikesForId:pagination.nextMaxId];
            
        } else {
            [self.refreshUploadingCounter setPercents:100 ForFetchRequestIndex:3];
            [self percentLabelRefresh];
            [self handleGetLikesResponse];
        }
        
    } failure:^(NSError *error) {
        _requests = 0;
        [self stopRefreshing];
    }];
}

#pragma mark - Actions

- (IBAction)onMenuButtonPressed:(id)sender {
    
    if (_tablewViewState == FullTableViewState) {
        _tablewViewState = PartTableViewState;
        
        self.rightCancelView.hidden = NO;
        
        __weak typeof(&*self)weakSelf = self;
        [UIView animateWithDuration:0.3f animations:^{
            weakSelf.rightCancelView.transform = CGAffineTransformMakeTranslation(0.8 * self.view.frame.size.width, 0);
            weakSelf.tableView.transform = CGAffineTransformMakeTranslation(0.8 * self.view.frame.size.width, 0);
            weakSelf.leftTableView.transform = CGAffineTransformMakeTranslation(0.8 * self.view.frame.size.width, 0);
            weakSelf.extraButton.transform = CGAffineTransformMakeTranslation(0.8 * self.view.frame.size.width, 0);
            weakSelf.userBackgroundImageView.transform = CGAffineTransformMakeTranslation(0.8 * self.view.frame.size.width, 0);
            weakSelf.leftMenuButton.transform = CGAffineTransformMakeRotation((90 * M_PI) / 180);
            
        } completion:^(BOOL finished) {}];
        
    } else {
        _tablewViewState = FullTableViewState;
        self.rightCancelView.hidden = YES;
        
        __weak typeof(&*self)weakSelf = self;
        [UIView animateWithDuration:0.3f animations:^{
            weakSelf.rightCancelView.transform = CGAffineTransformIdentity;
            weakSelf.tableView.transform = CGAffineTransformIdentity;
            weakSelf.leftTableView.transform = CGAffineTransformIdentity;
            weakSelf.extraButton.transform = CGAffineTransformIdentity;
            weakSelf.leftMenuButton.transform = CGAffineTransformIdentity;
            weakSelf.userBackgroundImageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {}];
    }
}

- (IBAction)onSearchButtonPressed:(id)sender {
    [self presentViewController:[MWSearchViewController new] animated:YES completion:nil];
}

#pragma mark - UI Settings

- (void)configureProgressHUD {

    self.topRefreshControl = [[UIRefreshControl alloc] init];
    self.topRefreshControl.tintColor = [UIColor whiteColor];
    [self.topRefreshControl addTarget:self action:@selector(refreshUserData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView insertSubview:self.topRefreshControl atIndex:0];
    
    if (self.tableView.contentOffset.y == 0)
        self.tableView.contentOffset = CGPointMake(0, -self.topRefreshControl.frame.size.height);
}

- (void)setTitleUpImage:(BOOL)isUp {
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:_lato_font_black size:11];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[[self.mainUser.user.username uppercaseString] stringByAppendingString:@"  "]];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = isUp ? [UIImage imageNamed:@"arrowDownTitleStatistics"] : [UIImage imageNamed:@"arrowTitleStatistics"];
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    
    [attributedString replaceCharactersInRange:NSMakeRange(attributedString.string.length - 1 , 1) withAttributedString:attrStringWithImage];
    label.attributedText = attributedString;
    [label sizeToFit];
    
    self.navigationItem.titleView = label;
}

#pragma mark - Refresh logic

- (void)refreshUserData {
    
    if (_isRefreshing == YES)
        return;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    _isRefreshing = YES;
    
    //userImageCellSource.isRefreshing = YES;
    //userImageCellSource.percents = 0;
    //((UserImageTableViewCell *)self.tableView.userImageCell).percentsLabel.text = [NSString stringWithFormat:@"%ld%%", (NSInteger)_refreshPercents];
    //[((UserImageTableViewCell *)self.tableView.userImageCell) setUpWithSource:userImageCellSource];
    
    [[InstagramAPI sharedInstance] getUser:self.mainUser.user.Id comp:^(IGUser *user) {
        
        MainUser *mainUser = [MainUser new];
        
        mainUser.isComplete = YES;
        user.password = self.mainUser.user.password;
        [mainUser setUser:user];
        [MainUser setMainUser:mainUser];
        self.mainUser = [MainUser mainUser];
        
        self.getLikesResponseArray = [NSMutableArray array];
        self.getFollowersResponseArray = [NSMutableArray array];
        self.getFollowingsResponseArray = [NSMutableArray array];
        
        [self getFollowersForId:@""];
        [self getLikesForId:@""];
        [self getFollowingsForId:@""];
        
    } failure:^(NSError *error) {
        _isRefreshing = NO;
        [self stopRefreshing];
    }];
}

- (void)checkForTableViewUpdate:(NSInteger)percents RequestNumber:(NSInteger)requestNumber {
    
    [self updateTopCellPercents:percents RequestNumber:requestNumber];
    
    if ([_refreshUploadingCounter getTotalPercent] == 100) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(waitWhileUserDataIsUpdatingInRealm) withObject:nil afterDelay:1];
            [self stopRefreshing];
        });
    }
}

- (void)waitWhileUserDataIsUpdatingInRealm {
    
    if (self.mainUser.photos.count) {
        IGMedia *media = self.mainUser.photos[0];
        if (media.image.standard_resolution) {
            [_userBackgroundImageView sd_setImageWithURL:[NSURL URLWithString:media.image.standard_resolution]];
        }
    }
    
    self.horizontalStaticCell.itemsArray = [MWDataCollector getItemsArrayForHorizontalCell];
    [self.horizontalStaticCell.collectionView reloadData];
    [self.tableView reloadData];
}

- (void)updateTopCellPercents:(CGFloat)percents RequestNumber:(NSInteger)requestNumber {
    
    [_refreshUploadingCounter setPercents:percents ForDBRequestIndex:requestNumber];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self percentLabelRefresh];
    });
}

- (void)percentLabelRefresh {
    _refreshPercents = [_refreshUploadingCounter getTotalPercent];
    //userImageCellSource.percents = _refreshPercents;
    //((UserImageTableViewCell *)self.tableView.userImageCell).percentsLabel.text = [NSString stringWithFormat:@"%ld%%", (NSInteger)_refreshPercents];
}

- (void)stopRefreshing {
    _isRefreshing = NO;
    [self.topRefreshControl endRefreshing];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_refreshUploadingCounter clear];
    _refreshPercents = 0;
}

#pragma mark - Navigation

- (void)transitionToControllerWithIndex:(NSInteger)index {
    
    BOOL showLockedItems = [AppUtils showLockedItems];
    
    switch (index) {
            
        case 0: {
            MWStalkersViewController *vc = [MWStalkersViewController new];
            vc.stalkersScreenType = NewFollowersStalkersScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 1: {
            MWStalkersViewController *vc = [MWStalkersViewController new];
            vc.stalkersScreenType = LostFollowersStalkersScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 2: {
            MWStalkersViewController *vc = [MWStalkersViewController new];
            vc.stalkersScreenType = DontFollowBackStalkersScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 3: {
            MWStalkersViewController *vc = [MWStalkersViewController new];
            vc.stalkersScreenType = NotFollowMeBackStalkersScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 4: {
            if (showLockedItems) {
                MWUpgradeViewController *vc = [MWUpgradeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = BlockingMeStalkersScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        } break;
            
        case 5: {
            if (showLockedItems) {
                MWUpgradeViewController *vc = [MWUpgradeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                MWDeletedLikesViewController *vc = [MWDeletedLikesViewController new];
                vc.screenType = MWDeletedLikesContentScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            }
        } break;
            
        case 6: {
            if (showLockedItems) {
                MWUpgradeViewController *vc = [MWUpgradeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                MWDeletedLikesViewController *vc = [MWDeletedLikesViewController new];
                vc.screenType = MWDeletedPhotosContentScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            }
        } break;
            
        case 7: {
            if (showLockedItems) {
                MWUpgradeViewController *vc = [MWUpgradeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = VisitorsStalkersScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            }
        } break;
            
        default: break;
    }
}

- (IBAction)onExtraButtonPressed:(id)sender {
    MWExtraViewController *vc = [MWExtraViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Database Update Logic

- (void)checkForUserInformationUpdate {
    
    if (_requests == 3) {
        _requests = 0;
        [self updateUserInfo];
    }
}

- (void)updateUserInfo {
    
    MainUser *previousUser = [MainUser previousUser];
    MainUser *mainUser = [MainUser mainUser];
    MainUser *storedUser = [MainUser storedUser];
    
    __weak typeof(&*self)weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    RLMThreadSafeReference *mainUserRef = [RLMThreadSafeReference referenceWithThreadConfined:mainUser];
    RLMThreadSafeReference *previousUserRef = [RLMThreadSafeReference referenceWithThreadConfined:previousUser];
    RLMThreadSafeReference *storedUserRef = [RLMThreadSafeReference referenceWithThreadConfined:storedUser];
    
    dispatch_async(queue, ^{
    
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        MainUser *newMainUser = [realm resolveThreadSafeReference:mainUserRef];
        MainUser *newPreviousUser = [realm resolveThreadSafeReference:previousUserRef];
        MainUser *newStoredUser = [realm resolveThreadSafeReference:storedUserRef];
        
        [MainUser addItemsFromArray:newMainUser.userFollowers notContainedIn:newPreviousUser.userFollowers toArray:newMainUser.userNewFollowers tag:1 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:1];
        }];
        
        [MainUser addItemsFromArray:newPreviousUser.userFollowers notContainedIn:newMainUser.userFollowers toArray:newMainUser.userLostFollowers tag:2 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:2];
        }];
        
        [MainUser addCommentsFromArray:newPreviousUser.comments notContainedIn:newMainUser.comments toArray:newMainUser.deletedComments CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:3];
        }];
        
        [MainUser addLikersFromArray:newPreviousUser.likes notContainedIn:newMainUser.likes toArray:newMainUser.deletedLikes tag:4 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:4];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowings notContainedIn:newPreviousUser.userFollowings toArray:newMainUser.userNewFollowings tag:5 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:5];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowers containedIn:newMainUser.userFollowings toArray:newMainUser.userMutualFollowers CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:6];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowings notContainedIn:newMainUser.userFollowers toArray:newMainUser.usersNotFollowMeBack tag:7 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:7];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowers notContainedIn:newMainUser.userFollowings toArray:newMainUser.userIAmNotFollowingBack tag:8 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:8];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowers notContainedIn:newStoredUser.userFollowers toArray:newStoredUser.userFollowers tag:9 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:9];
        }];
        
        [MainUser addItemsFromArray:newMainUser.userFollowings notContainedIn:newStoredUser.userFollowings toArray:newStoredUser.userFollowings tag:10 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:10];
        }];
        
        [MainUser addItemsFromArray:newStoredUser.userFollowings notContainedIn:newMainUser.userFollowings toArray:newStoredUser.userIAmNotFollowingBack tag:11 CompletionHandler:^(BOOL success, NSInteger progress) {
            if (success) [weakSelf checkForTableViewUpdate:progress RequestNumber:11];
        }];
    });
}

#pragma mark - Database Helpers

- (void)handleGetFollowersResponse {
    
    MainUser *mainUser = [MainUser mainUser];
    MainUser *previousUser = [MainUser previousUser];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    RLMThreadSafeReference *mainUserRef = [RLMThreadSafeReference referenceWithThreadConfined:mainUser];
    RLMThreadSafeReference *previousUserRef = [RLMThreadSafeReference referenceWithThreadConfined:previousUser];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        MainUser *newMainUser = [realm resolveThreadSafeReference:mainUserRef];
        MainUser *newPreviousUser = [realm resolveThreadSafeReference:previousUserRef];
        
        
        NSMutableDictionary *dates = [NSMutableDictionary dictionary];
        
        for (IGUser *user in newMainUser.userFollowers) {
            if (user.created_date)
                [dates setObject:user.created_date forKey:user.Id];
        }
        
        [[newMainUser.userFollowers realm] beginWriteTransaction];
        [newMainUser.userFollowers removeAllObjects];
        [[newMainUser.userFollowers realm] commitWriteTransaction];
        
        for (IGUser *user in self.getFollowersResponseArray) {

            [[newMainUser.userFollowers realm] beginWriteTransaction];
            if (dates[user.Id]) user.created_date = dates[user.Id];
            else user.created_date = [NSDate date];
            [[newMainUser.userFollowers realm] commitWriteTransaction];
                
            [MainUser addItem:user toArray:newMainUser.userFollowers];
        }
        
        long dateTimeInterval = (long)[[NSDate date] timeIntervalSince1970];
        
        if (newPreviousUser.userFollowers.count == 0 || [newPreviousUser.createdTime longLongValue] + (7 * 24 * 60 * 60) <= dateTimeInterval) {
            [[newPreviousUser.userFollowers realm] beginWriteTransaction];
            newPreviousUser.createdTime = [NSString stringWithFormat:@"%ld", dateTimeInterval];
            [newPreviousUser.userFollowers removeAllObjects];
            [newPreviousUser.userFollowers addObjects:newMainUser.userFollowers];
            [[newPreviousUser.userFollowers realm] commitWriteTransaction];
        }
        
        _requests++;
        dispatch_async(dispatch_get_main_queue(), ^{ [self checkForUserInformationUpdate]; });
    });
}

- (void)handleGetFollowingsResponse {
    
    MainUser *mainUser = [MainUser mainUser];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    RLMThreadSafeReference *mainUserRef = [RLMThreadSafeReference referenceWithThreadConfined:mainUser];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        MainUser *newMainUser = [realm resolveThreadSafeReference:mainUserRef];
        
        // Add new objects
        
        for (IGUser *user in self.getFollowingsResponseArray) {
            BOOL exist = NO;
            
            for (IGUser *user2 in newMainUser.userFollowings) {
                if ([user.Id isEqualToString:user2.Id]) {
                    exist = YES; break;
                }
            }
            
            if (!exist) {
                [[newMainUser.userFollowings realm] beginWriteTransaction];
                if (!user.created_date) user.created_date = NSDate.date;
                [[newMainUser.userFollowings realm] commitWriteTransaction];
                
                [MainUser addItem:user toArray:newMainUser.userFollowings];
            }
        }
        
        // Remove missing objects
        
        for (IGUser *user in newMainUser.userFollowings) {
            BOOL exist = NO;
            
            for (IGUser *user2 in self.getFollowingsResponseArray) {
                if ([user.Id isEqualToString:user2.Id]) {
                    exist = YES; break;
                }
            }
            
            if (!exist) {
                [[newMainUser.userFollowings realm] beginWriteTransaction];
                [[newMainUser.userFollowings realm] deleteObject:user];
                [[newMainUser.userFollowings realm] commitWriteTransaction];
            }
        }
        
        _requests++;
        dispatch_async(dispatch_get_main_queue(), ^{ [self checkForUserInformationUpdate]; });
    });
}

- (void)handleGetLikesResponse {
    
    MainUser *mainUser = [MainUser mainUser];
    MainUser *previousUser = [MainUser previousUser];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    RLMThreadSafeReference *mainUserRef = [RLMThreadSafeReference referenceWithThreadConfined:mainUser];
    RLMThreadSafeReference *previousUserRef = [RLMThreadSafeReference referenceWithThreadConfined:previousUser];
    
    dispatch_async(queue, ^{
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        MainUser *newMainUser = [realm resolveThreadSafeReference:mainUserRef];
        MainUser *newPreviousUser = [realm resolveThreadSafeReference:previousUserRef];
        
        NSMutableDictionary *dates = [NSMutableDictionary dictionary];
        
        for (IGLiker *liker in newMainUser.likes) {
            if (liker.created_date)
                [dates setObject:liker.created_date forKey:liker.mediaId];
        }
        
        [[newMainUser.likes realm] beginWriteTransaction];
        [newMainUser.likes removeAllObjects];
        [[newMainUser.likes realm] commitWriteTransaction];
        
        for (IGMedia *media in self.getLikesResponseArray) {
            for (IGLiker *liker in media.likes) {
                
                [[newMainUser.likes realm] beginWriteTransaction];
                if (dates[liker.mediaId]) liker.created_date = dates[liker.mediaId];
                //else liker.created_date = [NSDate date];
                [[newMainUser.likes realm] commitWriteTransaction];
                
                [MainUser addItem:liker toArray:newMainUser.likes];
            }
        }
        
        long dateTimeInterval = (long)[[NSDate date] timeIntervalSince1970];
        
        if (newPreviousUser.likes.count == 0 || [newPreviousUser.createdTime longLongValue] + (7 * 24 * 60 * 60) <= dateTimeInterval) {
            [[newPreviousUser.likes realm] beginWriteTransaction];
            newPreviousUser.createdTime = [NSString stringWithFormat:@"%ld", dateTimeInterval];
            [newPreviousUser.likes removeAllObjects];
            [newPreviousUser.likes addObjects:newMainUser.likes];
            [[newPreviousUser.likes realm] commitWriteTransaction];
        }
        
        
        // Add new objects
        
        for (IGMedia *media in self.getLikesResponseArray) {
            
            for (IGComment *comment in media.comments) {
                BOOL commentExist = NO;
                
                for (IGComment *comment2 in newMainUser.comments) {
                    if ([comment.Id isEqualToString:comment2.Id]) {
                        commentExist = YES; break;
                    }
                }
                if (!commentExist) [MainUser addItem:comment toArray:newMainUser.comments];
            }
            
            if ([media.type isEqualToString:@"photo"]) {
                BOOL exist = NO;
                
                for (IGMedia *media2 in newMainUser.photos) {
                    if ([media.Id isEqualToString:media2.Id]) {
                        exist = YES; break;
                    }
                }
                if (!exist) [MainUser addItem:media toArray:newMainUser.photos];
                
            } else {
                BOOL exist = NO;
                
                for (IGMedia *media2 in newMainUser.videos) {
                    if ([media.Id isEqualToString:media2.Id]) {
                        exist = YES; break;
                    }
                }
                if (!exist) [MainUser addItem:media toArray:newMainUser.videos];
            }
        }
        
        // Remove missing objects
        
        for (IGComment *comment in newMainUser.comments) {
            BOOL exist = NO;
            for (IGMedia *media in self.getLikesResponseArray) {
                for (IGComment *comment2 in media.comments) {
                    if ([comment.Id isEqualToString:comment2.Id]) {
                        exist = YES; break;
                    }
                }
            }
            
            if (!exist) {
                [[newMainUser.comments realm] beginWriteTransaction];
                [[newMainUser.comments realm] deleteObject:comment];
                [[newMainUser.comments realm] commitWriteTransaction];
            }
        }
        
        for (IGMedia *media in newMainUser.photos) {
            BOOL exist = NO;
            
            for (IGMedia *media2 in self.getLikesResponseArray) {
                if ([media.Id isEqualToString:media2.Id]) {
                    exist = YES; break;
                }
            }
            
            if (!exist) {
                [[newMainUser.photos realm] beginWriteTransaction];
                [[newMainUser.photos realm] deleteObject:media];
                [[newMainUser.photos realm] commitWriteTransaction];
            }
        }
        
        for (IGMedia *media in newMainUser.videos) {
            BOOL exist = NO;
            
            for (IGMedia *media2 in self.getLikesResponseArray) {
                if ([media.Id isEqualToString:media2.Id]) {
                    exist = YES; break;
                }
            }
            
            if (!exist) {
                [[newMainUser.videos realm] beginWriteTransaction];
                [[newMainUser.videos realm] deleteObject:media];
                [[newMainUser.videos realm] commitWriteTransaction];
            }
        }
        
        _requests++;
        dispatch_async(dispatch_get_main_queue(), ^{ [self checkForUserInformationUpdate]; });
    });
}

@end
