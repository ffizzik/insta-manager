//
//  UserPhotosCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "UserPhotosCell.h"
#import "AppUtils.h"
#import "PhotoItemCell.h"
#import "AppConstants.h"
#import "InstagramAPI.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>

@implementation UserPhotosCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if (_bottomRefreshControl == nil) {
        _bottomRefreshControl = [[UIRefreshControl alloc] init];
        [_bottomRefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        self.collectionView.bottomRefreshControl = _bottomRefreshControl;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"PhotoItemCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"PhotoItemCell"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
    }
    
    _itemsArray = [NSMutableArray array];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    if (refreshControl.tag != 0) {
        [refreshControl endRefreshing];
        return;
    }}

#pragma mark <UICollectionViewDataSource>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1, 0, 1, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = (collectionView.frame.size.width - 6) / 3.0f;
    return CGSizeMake(width, width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _itemsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PhotoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoItemCell" forIndexPath:indexPath];
    //cell.target = _source.target;
    [cell setupWithItem:_itemsArray[indexPath.row]];
    return cell;
}

@end
