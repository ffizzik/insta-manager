//
//  StalkersCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import <ImoDynamicTableView/ImoDynamicDefaultCell.h>

@interface StalkersCellSource : IDDCellSource

@property (nonatomic,retain) UIColor * backgroundColor;

@end

@interface StalkersCell : ImoDynamicDefaultCellExtended

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (nonatomic,strong) id target;

- (void)setUpWithSource:(StalkersCellSource *)source;

@end

