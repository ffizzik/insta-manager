//
//  Defines.h
//  Instatistics
//
//  Created by . on 24/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#ifndef Defines_h
#define Defines_h


#define UIColorFromRGBA(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

// Colors in HEX

#define COLOR_AZURE                           0x00a6ff
#define COLOR_LIGHT_AZURE                     0x15a4fa

// NEW Colors

#define COLOR_DARK_PURPLE                     0x16182e
#define COLOR_DARK_PURPLE_2                   0x46486C

#define COLOR_LEFT_GRADIENT_DARK_PURPLE       0x131628
#define COLOR_RIGHT_GRADIENT_DARK_PURPLE      0x191b3b

#define COLOR_LEFT_GRADIENT_PURPLE            0x2e1e6d
#define COLOR_RIGHT_GRADIENT_PURPLE           0x191b3d

#define COLOR_GREEN                           0x07b76b
#define COLOR_RED                             0xf13939



#define leftMainMenuBackgrounColor            0x141534
#define leftMainMenuColor                     0x14162F

#define subscriptionOrangeBackgroundColor     0xc86231
#define subscriptionYellowBackgroundColor     0xc8a631
#define subscriptionBlueBackgroundColor       0x53a4f8

#define subscriptionPurpleTextColor           0xc2bbf2

#define followersGainedGreenColor             0x2F8475
#define followersGainedPurpleColor            0x5660A0

#define extraProfileStalkersColor             0x52C7CA
#define extraEngagementColor                  0x6C6FFF
#define extraInsightsColor                    0x7A82AC

#define profileStalkerBlueColor               0x39c0e2
#define upgradePurpleColor                    0xC5BEF7

#define stalkerSelectedGreenColor             0x1B3246
#define stalkerRedColor                       0xFC2746


#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

#endif /* Defines_h */
