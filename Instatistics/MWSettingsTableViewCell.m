//
//  MWSettingsTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWSettingsTableViewCell.h"

@implementation MWSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
