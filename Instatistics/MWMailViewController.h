//
//  MWMailViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 11/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

typedef NS_ENUM(NSUInteger, MWMailScreenType) {
    GiveFeedbackMailScreenType = 0,
    ReportProblemScreenType
};

@interface MWMailViewController : MFMailComposeViewController

@property (assign, nonatomic) MWMailScreenType screenType;

@end
