//
//  MWSearchPlaceholderTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 28/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWSearchPlaceholderTableViewCell.h"

@interface MWSearchPlaceholderTableViewCell()

@property (strong, nonatomic) UIImageView *gradientView;

@end

@implementation MWSearchPlaceholderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gradientView = [[UIImageView alloc] initWithFrame:self.frame];
    self.gradientView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_PURPLE, 1.f) Frame:self.gradientView.bounds];
    [self insertSubview:self.gradientView atIndex:0];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.gradientView.frame = self.frame;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
