//
//  HTTPRequestType.h
//  Post Gallery
//
//  Created by . on 29/07/16.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#ifndef HTTPRequestType_h
#define HTTPRequestType_h

/**
 @author Denis Svichkarev
 
 Enumeration with different http requests types
 */

typedef NS_ENUM (NSUInteger, HTTPRequestType) {
    /**
     @author Denis Svichkarev
     
     GET request
     */
    HTTPGetRequestType = 0,
    /**
     @author Denis Svichkarev
     
     POST request
     */
    HTTPPostRequestType = 1,
    /**
     @author Denis Svichkarev
     
     PUT request
     */
    HTTPPutRequestType = 2,
    /**
     @author Denis Svichkarev
     
     DELETE request
     */
    HTTPDeleteRequestType = 3
};

#endif /* HTTPRequestType_h */
