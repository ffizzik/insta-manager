//
//  LeftMenuItemCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import <ImoDynamicTableView/ImoDynamicDefaultCell.h>
#import "InstagramAPI.h"

@interface LeftMenuItemCellSource : IDDCellSource

@property (nonatomic,retain) UIColor        *backgroundColor;
@property (nonatomic,retain) NSDictionary   *itemInfo;
@property (nonatomic,retain) UIColor        *titleColor;
@property (nonatomic, assign) BOOL          isSelected;

@end

@interface LeftMenuItemCell : ImoDynamicDefaultCellExtended

@property (weak, nonatomic) IBOutlet UIView *leftBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIImageView    *backView;
@property (weak, nonatomic) IBOutlet UIButton       *selectButton;
@property (weak, nonatomic) IBOutlet UILabel        *nameLabel;

- (void)setUpWithSource:(LeftMenuItemCellSource *)source;

@end

