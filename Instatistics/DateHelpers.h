//
//  PFDateHelpers.h
//  
//
//  Created by . on 09/08/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelpers : NSObject

/* 
 * Using format yyyy-MM-dd'T'HH:mm:ss'Z'
 * to "August 2nd 2015, 12 pm"
 */
+ (NSString *)getPrettyDateStringFromString:(NSString *)string;

+ (NSDate *)getDateFromString:(NSString *)dateString;

+ (NSString *)getStringFromDate:(NSDate *)date;

+ (NSString *)getDateStringFromTimestamp:(NSInteger)timestamp;

@end
