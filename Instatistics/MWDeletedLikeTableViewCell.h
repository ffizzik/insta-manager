//
//  MWDeletedLikeTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 02/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MWDeletedLikeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
