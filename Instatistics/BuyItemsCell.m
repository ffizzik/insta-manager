//
//  BuyItemsCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BuyItemsCell.h"
#import "AppUtils.h"


@implementation BuyItemsCell
    
- (void)awakeFromNib {
    [super awakeFromNib];
    
    _topRoundView.layer.cornerRadius = _topRoundView.frame.size.height / 2;
    _bottomRoundView.layer.cornerRadius = _bottomRoundView.frame.size.height / 2;
}
    
/*- (void)setUpWithSource:(BuyItemsCellSource *)source {
    self.backgroundColor = source.backgroundColor;
    
    _titleLabel.text = source.titleString;
    _priceLabel.text = source.priceString;
    _bigPriceLabel.text = source.bigPriceString;
    _mainView.backgroundColor = source.mainViewColor;
    
    _selectButton.tag = source.cellTag;
    [_selectButton removeTarget:source.target action:source.selector forControlEvents:UIControlEventAllEvents];
    [_selectButton addTarget:source.target action:source.selector forControlEvents:UIControlEventTouchUpInside];
    
    _topRoundView.layer.cornerRadius = _topRoundView.frame.size.height / 2;
    _bottomRoundView.layer.cornerRadius = _bottomRoundView.frame.size.height / 2;
}*/

@end




