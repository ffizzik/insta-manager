//
//  HorisontalMenuItemView.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 12/9/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorisontalMenuItem.h"

@interface HorisontalMenuItemView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
-(CGFloat)setupWithItem:(HorisontalMenuItem*)item;

@end
