//
//  MWPhotoCollectionViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGMedia.h"

@interface MWPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UILabel *likesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLabel;

- (void)setupWithItem:(IGMedia *)media;

@end
