//
//  MWExtraViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWExtraViewController.h"
#import "MWExtraTableViewCell.h"
#import "MWSearchViewController.h"
#import "MWStalkersViewController.h"
#import "MWUpgradeViewController.h"
#import "MWEngagementViewController.h"
#import "MWInsightPackViewController.h"

static NSString *extraTableViewCellID = @"MWExtraTableViewCell";

@interface MWExtraViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

#pragma mark - Controller Life Cycle

@implementation MWExtraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:extraTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:extraTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Extra";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
    
    UIImage *searchImage = [UIImage imageNamed:@"Icon - Search"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    searchButton.bounds = CGRectMake(0, 0, 24, 24);
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    UIBarButtonItem *searchBI = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    [searchButton addTarget:self action:@selector(onSearchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = searchBI;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL showLockedItems = [AppUtils showLockedItems];
    
    switch (indexPath.row) {
        case 0: {
            if (showLockedItems) {
                MWUpgradeViewController *vc = [MWUpgradeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
        
            } else {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = VisitorsStalkersScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        } break;
            
        case 1: {
            MWEngagementViewController *vc = [MWEngagementViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 2: {
            MWInsightPackViewController *vc = [MWInsightPackViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } break;
        
        default: break;
    }
}

- (void)onSearchButtonPressed:(id)sender {
    [self presentViewController:[MWSearchViewController new] animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MWExtraTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:extraTableViewCellID];
    
    switch (indexPath.row) {
        case 0: {
            cell.titleLabel.text = @"PROFILE STALKERS";
            cell.iconImageView.image = [UIImage imageNamed:@"extra_profile_stalkers"];
            cell.mainView.backgroundColor = UIColorFromRGBA(extraProfileStalkersColor, 1.f);
            
        } break;
            
        case 1: {
            cell.titleLabel.text = @"ENGAGEMENT";
            cell.iconImageView.image = [UIImage imageNamed:@"extra_engagement"];
            cell.mainView.backgroundColor = UIColorFromRGBA(extraEngagementColor, 1.f);
            
        } break;
            
        case 2: {
            cell.titleLabel.text = @"INSIGHTS";
            cell.iconImageView.image = [UIImage imageNamed:@"extra_insights"];
            cell.mainView.backgroundColor = UIColorFromRGBA(extraInsightsColor, 1.f);
            
        } break;
            
        default: break;
    }
    
    return cell;
}

@end
