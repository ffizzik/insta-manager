//
//  SpaceCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "SpaceCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation SpaceCellSource

- (id)init {
    
  if (self = [super init]) {
      self.cellClass = @"SpaceCell";
      self.backgroundColor = [UIColor clearColor];
      self.multipleSelection = YES;
      self.staticHeightForCell = 20;
  }
  return self;
}

@end

@implementation SpaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setUpWithSource:(SpaceCellSource *)source {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = source.backgroundColor;
}

@end




