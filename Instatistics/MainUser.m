//
//  MainUser.m
//  DeepFollowers Tracker
//
//  Created by Midnight.Works iMac on 12/13/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "MainUser.h"
#import "RegisteredUser.h"
#import "MainId.h"

@implementation MainUser

#pragma mark - Getters

+ (MainUser *)mainUser {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@", [MainId mainId]];
    RLMResults *allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults *result = [allRegisteredUsers objectsWithPredicate:predicate];
    RegisteredUser *registeredUser = [result firstObject];
    MainUser *mainUser = registeredUser.currentUser;
    return mainUser;
}

+ (MainUser *)firstRegisteredUser {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@", [MainId mainId]];
    RLMResults *allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults *result = [allRegisteredUsers objectsWithPredicate:predicate];
    RegisteredUser *registeredUser = [result firstObject];
    MainUser *mainUser = registeredUser.firstRegisteredUser;
    return mainUser;
}

+ (MainUser *)previousUser {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@", [MainId mainId]];
    RLMResults *allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults *result = [allRegisteredUsers objectsWithPredicate:predicate];
    RegisteredUser *registeredUser = [result firstObject];
    MainUser *previousUser = registeredUser.previousUser; 
    return previousUser;
}

+ (MainUser *)storedUser {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@", [MainId mainId]];
    RLMResults *allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults *result = [allRegisteredUsers objectsWithPredicate:predicate];
    RegisteredUser *registeredUser = [result firstObject];
    MainUser *storedUser = registeredUser.storedUser;
    return storedUser;
}

#pragma mark - Actions

+ (void)setUser:(IGUser *)user {
    MainUser *mainUser = [MainUser mainUser];
    [[mainUser realm] beginWriteTransaction];
    mainUser.user = user;
    [[mainUser realm] commitWriteTransaction];
}

+ (void)setMainUser:(MainUser *)user {
    
    if (user == nil)
        return;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@", user.user.Id];
    RLMResults *allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults *result = [allRegisteredUsers objectsWithPredicate:predicate];
    
    [[RLMRealm defaultRealm] beginWriteTransaction];
    
    long dateTimeInterval = (long)[[NSDate date] timeIntervalSince1970];
    user.createdTime = [NSString stringWithFormat:@"%ld", dateTimeInterval];
    
    if (result.count) {
        RegisteredUser *registeredUser = [result firstObject];
        
        if ([registeredUser.previousUser.createdTime longLongValue] + (7 * 24 * 60 * 60) <= dateTimeInterval) {
            registeredUser.previousUser = [[MainUser alloc] initWithValue:user];
        }
        
        if (!registeredUser.storedUser)
            registeredUser.storedUser = [[MainUser alloc] initWithValue:user];
        
        if (!registeredUser.currentUser) registeredUser.currentUser = [[MainUser alloc] initWithValue:user];
        else registeredUser.currentUser.user = user.user;
        
    } else {
        RegisteredUser *registeredUser = [RegisteredUser new];
        registeredUser.userId = user.user.Id;
        registeredUser.previousUser = [[MainUser alloc] initWithValue:user];
        registeredUser.currentUser = [[MainUser alloc] initWithValue:user];
        registeredUser.storedUser = [[MainUser alloc] initWithValue:user];
        [[RLMRealm defaultRealm] addObject:registeredUser];
    }
    
    RLMResults *mainIds = [MainId allObjects];
    
    if (mainIds.count == 0) {
        MainId *mainId = [MainId new];
        mainId.mainId = user.user.Id;
        [[RLMRealm defaultRealm] addObject:mainId];
        
    } else {
        MainId *mainId = [mainIds firstObject];
        mainId.mainId = user.user.Id;
    }
    
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)removeAllObjects {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
}

+ (void)addItem:(RLMObject *)item toArray:(RLMArray *)array {
    [[array realm] beginWriteTransaction];
    [array addObject:item];
    [[array realm] commitWriteTransaction];
}

+ (void)addItems:(NSArray *)items toArray:(RLMArray *)array {
    [[array realm] beginWriteTransaction];
    [array addObjects:items];
    [[array realm] commitWriteTransaction];
}

+ (void)removeUser:(MainUser *)user {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.userId == %@",user.user.Id];
    RLMResults * allRegisteredUsers = [RegisteredUser allObjects];
    RLMResults * result = [allRegisteredUsers objectsWithPredicate:predicate];
    [[RLMRealm defaultRealm] beginWriteTransaction];
    RegisteredUser *registeredUser = [result firstObject];
    [[registeredUser realm] deleteObject:registeredUser];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)removeAllObjectsFromArray:(RLMArray *)array {
    [[array realm] beginWriteTransaction];
    [array removeAllObjects];
    [[array realm] commitWriteTransaction];
}

#pragma mark - Adding items to MainUser

+ (void)addItemsFromArray:(RLMArray *)newArray
              containedIn:(RLMArray *)oldArray
                  toArray:(RLMArray *)result
        CompletionHandler:(void (^)(BOOL success, NSInteger progress))completionHandler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    RLMThreadSafeReference *newArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:newArray];
    RLMThreadSafeReference *oldArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:oldArray];
    RLMThreadSafeReference *resultRef = [RLMThreadSafeReference referenceWithThreadConfined:result];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        RLMArray *newArray = [realm resolveThreadSafeReference:newArrayRef];
        RLMArray *oldArray = [realm resolveThreadSafeReference:oldArrayRef];
        RLMArray *result = [realm resolveThreadSafeReference:resultRef];
        
        if (!result || !newArray || !oldArray) {
            completionHandler(NO, 100);
            return;
        }
        
        [MainUser removeAllObjectsFromArray:result];
        
        NSInteger i = 0, j = 0;
        NSInteger period = newArray.count / 4;
        
        for (IGUser *user in newArray) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.Id == %@",user.Id];
            
            if ([oldArray objectsWithPredicate:predicate].count > 0) {
                [[result realm] beginWriteTransaction];
                if (!user.created_date) user.created_date = NSDate.date;
                [[result realm] commitWriteTransaction];
                
                [MainUser addItem:user toArray:result];
            }
            
            i++;
            
            if (i == period) {
                j++;
                completionHandler(YES, (i * j) / newArray.count);
                i = 0;
            }
        }
        
        completionHandler(YES, 100);
    });
}

+ (void)addItemsFromArray:(RLMArray *)newArray
           notContainedIn:(RLMArray *)oldArray
                  toArray:(RLMArray *)result
                      tag:(NSInteger)tag
        CompletionHandler:(void (^)(BOOL success, NSInteger progress))completionHandler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    RLMThreadSafeReference *newArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:newArray];
    RLMThreadSafeReference *oldArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:oldArray];
    RLMThreadSafeReference *resultRef = [RLMThreadSafeReference referenceWithThreadConfined:result];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        RLMArray *newArray = [realm resolveThreadSafeReference:newArrayRef];
        RLMArray *oldArray = [realm resolveThreadSafeReference:oldArrayRef];
        RLMArray *result = [realm resolveThreadSafeReference:resultRef];
        
        if (!result || !newArray || !oldArray) {
            completionHandler(NO, 100);
            return;
        }
        
        if (!(tag == 9 || tag == 10 || tag == 11)) { // Accumulate followers data
            [MainUser removeAllObjectsFromArray:result];
        }
            
        if (!result || !newArray || !oldArray) {
            completionHandler(NO, 100);
            return;
        }
        
        NSInteger i = 0, j = 0;
        NSInteger period = newArray.count / 4;

        
        for (IGUser *user in newArray) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.Id == %@", user.Id];
            
            if ([oldArray objectsWithPredicate:predicate].count == 0) {
                [[result realm] beginWriteTransaction];
                if (!user.created_date) user.created_date = NSDate.date;
                [[result realm] commitWriteTransaction];

                [MainUser addItem:user toArray:result];
            }
            
            i++;
            
            if (i == period) {
                j++;
                completionHandler(YES, (i * j) / newArray.count);
                i = 0;
            }
        }
        
        completionHandler(YES, 100);
    });
}

+ (void)addCommentsFromArray:(RLMArray *)newArray
              notContainedIn:(RLMArray *)oldArray
                     toArray:(RLMArray *)result
           CompletionHandler:(void (^)(BOOL success, NSInteger progress))completionHandler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    RLMThreadSafeReference *newArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:newArray];
    RLMThreadSafeReference *oldArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:oldArray];
    RLMThreadSafeReference *resultRef = [RLMThreadSafeReference referenceWithThreadConfined:result];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        RLMArray *newArray = [realm resolveThreadSafeReference:newArrayRef];
        RLMArray *oldArray = [realm resolveThreadSafeReference:oldArrayRef];
        RLMArray *result = [realm resolveThreadSafeReference:resultRef];
        
        if (!result || !newArray || !oldArray) {
            completionHandler(NO, 100);
            return;
        }
        
        [MainUser removeAllObjectsFromArray:result];
        
        NSInteger i = 0, j = 0;
        NSInteger period = newArray.count / 4;
        
        for (IGComment *comment in newArray) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.Id == %@",comment.Id];
            
            if ([oldArray objectsWithPredicate:predicate].count == 0)
                [MainUser addItem:comment toArray:result];
            
            i++;
            
            if (i == period) {
                j++;
                completionHandler(YES, (i * j) / newArray.count);
                i = 0;
            }
        }
        
        completionHandler(YES, 100);
    });
}

+ (void)addLikersFromArray:(RLMArray *)newArray
            notContainedIn:(RLMArray *)oldArray
                   toArray:(RLMArray *)result
                       tag:(NSInteger)tag
         CompletionHandler:(void (^)(BOOL success, NSInteger progress))completionHandler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    RLMThreadSafeReference *newArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:newArray];
    RLMThreadSafeReference *oldArrayRef = [RLMThreadSafeReference referenceWithThreadConfined:oldArray];
    RLMThreadSafeReference *resultRef = [RLMThreadSafeReference referenceWithThreadConfined:result];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        RLMArray *newArray = [realm resolveThreadSafeReference:newArrayRef];
        RLMArray *oldArray = [realm resolveThreadSafeReference:oldArrayRef];
        RLMArray *result = [realm resolveThreadSafeReference:resultRef];
        
        if (!result || !newArray || !oldArray) {
            completionHandler(NO, 100);
            return;
        }
        
        //if (tag != 4) { // Accumulate likes data
            [MainUser removeAllObjectsFromArray:result];
        //}
        
        NSInteger i = 0, j = 0;
        NSInteger period = newArray.count / 4;
        
        for (IGLiker *liker in newArray) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.user.Id == %@ AND self.mediaId == %@", liker.user.Id, liker.mediaId];
            
            if ([oldArray objectsWithPredicate:predicate].count == 0) {
                [[result realm] beginWriteTransaction];
                if (!liker.created_date) liker.created_date = NSDate.date;
                [[result realm] commitWriteTransaction];
            
                [MainUser addItem:liker toArray:result];
            }
            
            i++;
            
            if (i == period) {
                j++;
                completionHandler(YES, (i * j) / newArray.count);
                i = 0;
            }
        }
        
        completionHandler(YES, 100);
    });
}

@end
