//
//  LeftTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 26/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIView *leftWhiteView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
