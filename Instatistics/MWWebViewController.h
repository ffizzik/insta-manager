//
//  MWWebViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 11/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MWWebScreenType) {
    TermsOfUseWebScreenType = 0,
    AboutSubscriptionsWebScreenType,
    FollowInstagramWebScreenType
};

@interface MWWebViewController : UIViewController

@property (assign, nonatomic) MWWebScreenType screenType;

@end
