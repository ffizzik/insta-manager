//
//  UITableView+MWHelpers.h
//  
//
//  Created by Denis Svichkarev on 04.03.16.
//
//

#import <Foundation/Foundation.h>

@interface UITableView (MWHelpers)

- (void)adjustInsetWithBottomHeight:(CGFloat)bottomHeight;

@end
