//
//  UserImageTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 24/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "UserImageTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UserImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //_userImageView.layer.cornerRadius = _userImageView.frame.size.height / 2;
    //_innerView.layer.cornerRadius = _innerView.frame.size.height / 2;
    _innerView.layer.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0.2 alpha:0.3].CGColor;
    
    self.innerView.hidden = YES;
    self.percentsLabel.hidden = YES;
}

/*- (void)setUpWithSource:(UserImageTableViewCellSource *)source {
    
    self.backgroundColor = source.backgroundColor;
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:source.userImageURL]];
    
    if (source.isRefreshing) {
        
        self.userImageView.hidden = YES;
        self.innerView.hidden = NO;
        self.percentsLabel.hidden = NO;
        
        self.percentsLabel.text = [NSString stringWithFormat:@"%ld%%", source.percents];
        
    } else {
        self.userImageView.hidden = NO;
        self.innerView.hidden = YES;
        self.percentsLabel.hidden = YES;
    }
}*/

@end

