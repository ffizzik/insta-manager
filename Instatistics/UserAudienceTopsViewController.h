//
//  UserAudienceTopsViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import "BaseTableViewController.h"

@interface UserAudienceTopsViewController : BaseTableViewController

@property (nonatomic,assign) NSInteger selectedItem;
@property (nonatomic,retain) NSString* selectedItemId;

@end
