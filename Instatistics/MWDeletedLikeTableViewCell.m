//
//  MWDeletedLikeTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 02/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWDeletedLikeTableViewCell.h"

@implementation MWDeletedLikeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height / 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
