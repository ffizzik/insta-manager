//
//  LeftMenuItemCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "LeftMenuItemCell.h"
#import "AppUtils.h"
#import "UserStatisticsViewController.h"

@implementation LeftMenuItemCellSource

- (id)init {
  self = [super init];
  if (self)
  {
    self.cellClass = @"LeftMenuItemCell";
      self.backgroundColor = [AppUtils separatorsColor];
      self.staticHeightForCell = 60.0f;
      self.titleColor = [UIColor whiteColor];
  }
  return self;
}

@end


@implementation LeftMenuItemCell

- (void)setUpWithSource:(LeftMenuItemCellSource *)source {
    
    _leftBackgroundView.backgroundColor = source.isSelected ? UIColorFromRGB(leftMainMenuColor) : [UIColor clearColor];
    _nameLabel.textColor = source.isSelected ? [UIColor whiteColor] : source.titleColor;
    _leftView.hidden = source.isSelected? NO : YES;
    
    NSString *titleString = [source.itemInfo[@"title"] uppercaseString];
   // NSString *subTitleString = source.itemInfo[@"subtitle"];
   // NSString *fullString = titleString;
    
//    if ([AppUtils isValidObject:subTitleString]) {
//        fullString = [fullString stringByAppendingFormat:@"\n%@",subTitleString];
//    }

    //_nameLabel.font = [UIFont fontWithName:_nameLabel.font.fontName size:[source.itemInfo[@"itemTag"] integerValue] == 4 ? 12 : 16];
    
    //UserStatisticsViewController *statistics = (UserStatisticsViewController*)source.target;
    
    _nameLabel.text = titleString;
    
    _selectButton.tag = [source.itemInfo[@"itemTag"] integerValue];
    //[_selectButton removeTarget:source.target action:source.selector forControlEvents:UIControlEventAllEvents];
    [_selectButton addTarget:source.target action:source.selector forControlEvents:UIControlEventTouchUpInside];
}

@end




