//
//  MWSettingsViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWSettingsViewController.h"
#import "MWGradientHeaderTableViewCell.h"
#import "MWSearchViewController.h"
#import "MWSettingsTableViewCell.h"
#import "MWUpgradeViewController.h"
#import "MWMailViewController.h"
#import "MWWebViewController.h"
#import "MWNotificationsViewController.h"

#import "MidnightRate.h"
#import <StoreKit/StoreKit.h>
#import "MainUser.h"
#import <SVProgressHUD/SVProgressHUD.h>


static NSString *gradientHeaderTableViewCellID = @"MWGradientHeaderTableViewCell";
static NSString *settingsTableViewCellID = @"MWSettingsTableViewCell";

@interface MWSettingsViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MWSettingsViewController {
    NSString *selectedProduct;
    NSString *paidProduct;
}

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:gradientHeaderTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:gradientHeaderTableViewCellID];
    [self.tableView registerNib:[UINib nibWithNibName:settingsTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:settingsTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Settings";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1 || indexPath.row == 6 || indexPath.row == 9)
        return;
    
    switch (indexPath.row) {
            
        case 0: {
            [SVProgressHUD show];
            [self restore];
        } break;
            
            /* ------------------------------- */
            
        case 2: {
            MainUser *mainUser = [MainUser mainUser];
            
            [UINavigationBar appearance].barStyle = UIBarStyleDefault;
            [UINavigationBar appearance].translucent = NO;
            [UINavigationBar appearance].tintColor = [UIColor whiteColor];
            [UINavigationBar appearance].barTintColor = UIColorFromRGB(COLOR_DARK_PURPLE);
            [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                 NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]};
            
            MWMailViewController *vc = [MWMailViewController new];
            vc.screenType = GiveFeedbackMailScreenType;
            vc.navigationBar.tintColor = [UIColor whiteColor];
            vc.navigationItem.title = @"Give Feedback";
            [vc setSubject:[NSString stringWithFormat:@"Give Feedback (%@)", mainUser.user.full_name]];
            [vc setToRecipients:@[@"support@netstick.com"]];
            [self.navigationController presentViewController:vc animated:YES completion:nil];
        } break;
            
        case 3: {
            MainUser *mainUser = [MainUser mainUser];
            
            [UINavigationBar appearance].barStyle = UIBarStyleDefault;
            [UINavigationBar appearance].translucent = NO;
            [UINavigationBar appearance].tintColor = [UIColor whiteColor];
            [UINavigationBar appearance].barTintColor = UIColorFromRGB(COLOR_DARK_PURPLE);
            [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                 NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]};
            
            MWMailViewController *vc = [MWMailViewController new];
            vc.screenType = ReportProblemScreenType;
            vc.navigationBar.tintColor = [UIColor whiteColor];
            vc.navigationItem.title = @"Report a Problem";
            [vc setSubject:[NSString stringWithFormat:@"Report a Problem (%@)", mainUser.user.full_name]];
            [vc setToRecipients:@[@"support@netstick.com"]];
            [self.navigationController presentViewController:vc animated:YES completion:nil];
        } break;
            
        case 4: {
            MWWebViewController *vc = [MWWebViewController new];
            vc.screenType = TermsOfUseWebScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 5: {
            MWWebViewController *vc = [MWWebViewController new];
            vc.screenType = AboutSubscriptionsWebScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
            /* ------------------------------- */
            
        case 7: {
            NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id1239065155?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        } break;
            
        case 8: {
            MWWebViewController *vc = [MWWebViewController new];
            vc.screenType = FollowInstagramWebScreenType;
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
            /* ------------------------------- */
            
        case 10: {
            MWNotificationsViewController *vc = [MWNotificationsViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 11: {
            
            __weak typeof(&*self)weakSelf = self;
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure to clear tracking history?" message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *button1 = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                MainUser *storedUser = [MainUser storedUser];
                MainUser *mainUser = [MainUser mainUser];
                
                [[storedUser.userFollowers realm] beginWriteTransaction];
                [storedUser.userFollowers removeAllObjects];
                [[storedUser.userFollowers realm] commitWriteTransaction];
                
                [[storedUser.userLostFollowers realm] beginWriteTransaction];
                [storedUser.userLostFollowers removeAllObjects];
                [[storedUser.userLostFollowers realm] commitWriteTransaction];
                
                [[storedUser.userIAmNotFollowingBack realm] beginWriteTransaction];
                [storedUser.userIAmNotFollowingBack removeAllObjects];
                [[storedUser.userIAmNotFollowingBack realm] commitWriteTransaction];
                
                [[storedUser.likes realm] beginWriteTransaction];
                [storedUser.likes removeAllObjects];
                [[storedUser.likes realm] commitWriteTransaction];
                
                
                
                [[storedUser.deletedLikes realm] beginWriteTransaction];
                [storedUser.deletedLikes removeAllObjects];
                [[storedUser.deletedLikes realm] commitWriteTransaction];
                
                [[mainUser.deletedLikes realm] beginWriteTransaction];
                [mainUser.deletedLikes removeAllObjects];
                [[mainUser.deletedLikes realm] commitWriteTransaction];
                
                
                
                [[mainUser.userFollowings realm] beginWriteTransaction];
                [mainUser.userFollowings removeAllObjects];
                [[mainUser.userFollowings realm] commitWriteTransaction];
                
                [[storedUser.userFollowings realm] beginWriteTransaction];
                [storedUser.userFollowings removeAllObjects];
                [[storedUser.userFollowings realm] commitWriteTransaction];
                
                [weakSelf performSelector:@selector(onHistoryClearedAction) withObject:nil afterDelay:2];
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            }];
            
            [alert addAction:cancelAction];
            [alert addAction:button1];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        } break;
            
        default: break;
    }
}

- (void)onHistoryClearedAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"History cleaned." message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *button1 = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:button1];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Restore Purchases";
            return cell;
        } break;
            
            /* ------------------------------- */
        
        case 1: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_info"] Title:@"Help"];
            return cell;
        } break;
            
        case 2: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Give Feedback";
            return cell;
        } break;
            
        case 3: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Report a Problem";
            return cell;
        } break;
            
        case 4: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Terms of Use & Privacy Policy";
            return cell;
        } break;
            
        case 5: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"About Subscriptions";
            return cell;
        } break;
            
            /* ------------------------------- */
            
        case 6: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_share"] Title:@"Share"];
            return cell;
        } break;
            
        case 7: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Review us on App Store";
            return cell;
        } break;
            
        case 8: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Follow us on Instagram";
            return cell;
        } break;
            
            /* ------------------------------- */
            
        case 9: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_settings"] Title:@"Settings"];
            return cell;
        } break;
            
        case 10: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Notifications";
            return cell;
        } break;
            
        case 11: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Clear History";
            return cell;
        } break;
            
            
        default: break;
    }
    
    MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
    
    return cell;
}

#pragma mark - Payment Actions

- (void)removeObservers {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        request.delegate = self;
        [self purchase:validProduct];
        
    } else if(!validProduct) {
        [SVProgressHUD dismiss];
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [self paymentQueue:queue updatedTransactions:queue.transactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    
    [SVProgressHUD dismiss];
    
    if (transactions.count == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:@"Your subscriptions were expired or never purchased" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {}];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch(transaction.transactionState) {
                
            case SKPaymentTransactionStateDeferred:
                break;
                
            case SKPaymentTransactionStatePurchasing:
                break;
                
            case SKPaymentTransactionStatePurchased: {
                paidProduct = selectedProduct;
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateRestored: {
                //paidProduct = transaction.originalTransaction.payment.productIdentifier;
                NSLog(@"Successfully restored");
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateFailed: {
                NSString *messageString = transaction.error.localizedDescription;
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:messageString preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Retry"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //[self buyItem:_tempSender];
                                           }];
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
        }
    }
}

- (void)notifySubscribtion:(SKPaymentTransaction *)transaction {
    
    [Storage setSubsciptionsStatus:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[MidnightRate sharedInstance] purchased];
    }];
}

@end
