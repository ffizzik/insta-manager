//
//  PFDateHelpers.m
//
//
//  Created by . on 09/08/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "DateHelpers.h"

@implementation DateHelpers

+ (NSString *)getPrettyDateStringFromString:(NSString *)string {
    return [DateHelpers getStringFromDate:[DateHelpers getDateFromString:string]];
}

+ (NSDate *)getDateFromString:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter1 dateFromString:dateString];
    
    if (!date) {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        date = [dateFormatter1 dateFromString:dateString];
    }
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date];
    
    return destinationDate;
}

+ (NSString *)getStringFromDate:(NSDate *)date {
    
    NSString *suffixString = [DateHelpers daySuffixForDate:date];
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    
    [dateFormatters setDateFormat:[NSString stringWithFormat:@"MMMM' 'dd'%@ 'yyyy', 'hh a", suffixString]];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    return [dateFormatters stringFromDate:date];
}

+ (NSString *)getDateStringFromTimestamp:(NSInteger)timestamp {

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return [DateHelpers getStringFromDate:date];
}

+ (NSString *)daySuffixForDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayOfMonth = [calendar component:NSCalendarUnitDay fromDate:date];
    switch (dayOfMonth) {
        case 1:
        case 21:
        case 31: return @"st";
        case 2:
        case 22: return @"nd";
        case 3:
        case 23: return @"rd";
        default: return @"th";
    }
}

@end
