//
//  MWMostLikedViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MWMediaScreenType) {
    MostPopularMediaScreenType,
    MostLikedMediaScreenType,
    MostCommentedMediaScreenType
};

@interface MWMostLikedViewController : UIViewController

@property (assign, nonatomic) MWMediaScreenType mediaScreenType;

@end
