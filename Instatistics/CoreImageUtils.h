//
//  CoreImageUtils.h
//  Instatistics
//
//  Created by Denis Svichkarev on 10/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreImageUtils : NSObject

+ (UIImage *)imageFromGradientStartColor:(UIColor *)startColor EndColor:(UIColor *)endColor Frame:(CGRect)frame;

+ (UIImage *)imageFromVerticalGradientStartColor:(UIColor *)startColor EndColor:(UIColor *)endColor Frame:(CGRect)frame;

@end
