//
//  NetworkFetcher.m
//  Instatistics
//
//  Created by . on 28/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "NetworkFetcher.h"
#import "AppConstants.h"
#import "DateHelpers.h"

@interface NetworkFetcher() <NSURLSessionDelegate>

@property (strong, nonatomic) NSURLSessionConfiguration *urlConfiguration;
@property (strong, nonatomic) NSURLSession *urlSession;
@property (assign, nonatomic) NSTimeInterval timeoutInterval;

+ (instancetype)sharedFetcher;

@end

@implementation NetworkFetcher

# pragma mark - Settings

static NetworkFetcher *sharedFetcher = nil;

+ (instancetype)sharedFetcher {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) [self setup];
    return self;
}

- (void)setup {
    self.urlConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.urlSession = [NSURLSession sessionWithConfiguration:self.urlConfiguration delegate:self delegateQueue:nil];
    self.timeoutInterval = 30.0;
}

#pragma mark - Requests

- (void)loginWithEmail:(NSString *)email Password:(NSString *)password Completion:(void (^)(BOOL result))completion {
    
    NSString *additionalUrl = [NSString stringWithFormat:@"%@config2.php", MAINLINK];
    
}

#pragma mark - In-App methods

- (void)checkInAppPurchaseStatusCompletion:(void (^)(BOOL result))completion {
    
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    
    if (receipt) {
        BOOL sandbox = [[receiptURL lastPathComponent] isEqualToString:@"sandboxReceipt"];
        
        NSDictionary *parameters = @{
                                        @"receipt-data"   : [receipt base64EncodedStringWithOptions:0],
                                        @"password"       : IN_APP_SECRET_KEY
                                    };
        
        NSString *storeURL = @"https://buy.itunes.apple.com/verifyReceipt";
        
        if (sandbox)
            storeURL = @"https://sandbox.itunes.apple.com/verifyReceipt";
        
        NSMutableURLRequest *request = [self requestWithURLString:storeURL requestType:HTTPPostRequestType Parameters:parameters TimeoutInterval:self.timeoutInterval];
        NSURLSessionDataTask *postDataTask = [self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *responseObject;
            if (data) responseObject = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:NSJSONReadingMutableContainers
                                                                         error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //NSLog(@"jsonResponse:%@", responseObject);
                
                if (responseObject[@"latest_receipt_info"]) {
                    NSDictionary *dictLatestReceiptsInfo = responseObject[@"latest_receipt_info"];
                    
                    long long int expirationDateMs = [[dictLatestReceiptsInfo valueForKeyPath:@"@max.expires_date_ms"] longLongValue];
                    long long requestDateMs = [responseObject[@"receipt"][@"request_date_ms"] longLongValue];
                    NSLog(@"ex:%@ - cur:%@", [DateHelpers getDateStringFromTimestamp:expirationDateMs / 1000], [DateHelpers getDateStringFromTimestamp:requestDateMs / 1000]);
                    completion([[responseObject objectForKey:@"status"] integerValue] == 0 && (expirationDateMs > requestDateMs));
                    
                } else
                    completion(NO);
            });
        }];
        
        [postDataTask resume];
        
    } else {
        completion(NO);
    }
}

#pragma mark - Helper methods

- (NSMutableURLRequest *)requestWithURLString:(NSString *)urlString
                                  requestType:(HTTPRequestType)httpRequestType
                                   Parameters:(NSDictionary *)parameters
                              TimeoutInterval:(NSTimeInterval)timeoutInterval {
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:timeoutInterval];
    
    [request addValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Accept"];
    
    switch (httpRequestType) {
        case HTTPGetRequestType:    [request setHTTPMethod:@"GET"];     break;
        case HTTPPostRequestType:   [request setHTTPMethod:@"POST"];    break;
        case HTTPPutRequestType:    [request setHTTPMethod:@"PUT"];     break;
        case HTTPDeleteRequestType: [request setHTTPMethod:@"DELETE"];  break;
        default: NSLog(@"Bad request type"); break;
    }
    
    if (parameters) {
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
        [request setHTTPBody:data];
    }
    return request;
}


@end
