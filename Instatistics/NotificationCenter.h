//
//  NotificationCenter.h
//  Facetate
//
//  Created by Denis Svichkarev on 9/04/2017.
//  Copyright © 2017 Denis Svichkarev. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kReminderNotificationRequest;
extern NSString * const kReminderNotification;


@interface NotificationCenter : NSObject

@property (assign, nonatomic) BOOL isGrantedNotificationAccess;

+ (instancetype)sharedNotificationCenter;

- (void)requestAuthorization;
- (void)isPendingNotificatonRequests:(void(^)(BOOL response))completion;

- (void)setNotificationsWithTimeInterval:(NSTimeInterval)timeInterval Repeat:(BOOL)repeat;
- (void)removeRemindNotifications;

- (BOOL)notificationsInstalled;

@end
