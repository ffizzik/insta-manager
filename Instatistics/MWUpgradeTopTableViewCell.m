//
//  MWUpgradeTopTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 04/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWUpgradeTopTableViewCell.h"

@interface MWUpgradeTopTableViewCell() <UIScrollViewDelegate>

@property (strong, nonatomic) NSArray *titlesArray;

@end

@implementation MWUpgradeTopTableViewCell {
    BOOL firstRun;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    firstRun = YES;
    
    _titlesArray = @[@{@"title" : @"",
                       @"description" : @"Subscribe to access the best engagement analytics and daily scan tool to help you grow your brand and influence."},
                     @{@"title" : @"Daily Scan",
                       @"description" :@"Never miss a beat. Let our cloud analytics refresh your data and followers, everyday, automatically."},
                     @{@"title" : @"Audience",
                       @"description" :@"Better understand your followers through location and behavioral data about them."},
                     @{@"title" : @"Engagement",
                       @"description" :@"Discover your best and most engaged followers. Reactivate ones that have gone “ghost”."},
                     @{@"title" : @"Loss Reports",
                       @"description" :@"Learn who is unfollowing or blocking you, posting and deleting comments, or even unliking your photos."},
                     @{@"title" : @"Multiple Accounts",
                       @"description" :@"Track followers and account analytics for up to 3 accounts."}
                     ];
    
    self.scrollView.delegate = self;
}

- (void)configure {
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    CGRect firstFrame = CGRectMake(0, 0, window.frame.size.width, self.scrollView.bounds.size.height);
    CGRect bigFrame = firstFrame;
    
    bigFrame.size.width *= 6;
    
    self.scrollView.contentSize = bigFrame.size;
    
    for (UIView *view in self.scrollView.subviews)
        if (view.tag == 200)
            [view removeFromSuperview];
    
    for (int i = 0; i < 6; i++) {
        
        CGFloat yPosition = 0.2;
        
        if ([AppUtils isIphoneVersion:5])
            yPosition = 0.1;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16 + firstFrame.origin.x + (i * firstFrame.size.width), yPosition * firstFrame.size.height, firstFrame.size.width - 32, firstFrame.size.height)];
        label.numberOfLines = 0;
        label.font = [UIFont fontWithName:_gothamPro_font_medium size:14.f];
        label.textColor = UIColorFromRGB(upgradePurpleColor);
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = 200;
        label.text = [_titlesArray[i] objectForKey:@"description"];
        [self.scrollView addSubview:label];
        
        [label setNeedsLayout];
    }
    
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (void)updateIndexView:(NSInteger)index {
    
    for (UIView *dotView in self.subviews)
        if (dotView.tag == 400)
            [dotView removeFromSuperview];
    
    for (int i = 0; i < 6; i++) {
        
        UIView *stripeView = [[UIView alloc] init];
        stripeView.backgroundColor = [UIColor whiteColor];
        
        if (index == i)  stripeView.alpha = 1;
        else             stripeView.alpha = 0.1;
        
        stripeView.tag = 400;
        
        stripeView.frame = CGRectMake((self.frame.size.width - 180) / 2 + (30 * i), self.frame.size.height * 0.9, 25, 2);
        [self addSubview:stripeView];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (firstRun) {
        firstRun = NO;
        [self updateIndexView:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    static NSInteger previousPage = 0;
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if (previousPage != page) {
        [self updateIndexView:page];
        previousPage = page;
    }
}

@end
