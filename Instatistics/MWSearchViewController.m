//
//  MWSearchViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 28/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWSearchViewController.h"
#import "MWStalkerTableViewCell.h"
#import "MWSearchPlaceholderTableViewCell.h"
#import "MWProfileStalkerViewController.h"
#import "AppConstants.h"
#import "AppDelegate.h"

#import "MainUser.h"
#import "UIImageView+AFNetworking.h"

static NSString *stalkerTableViewCellID = @"MWStalkerTableViewCell";
static NSString *searchPlaceholderTableViewCellID = @"MWSearchPlaceholderTableViewCell";

@interface MWSearchViewController () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;
    
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;
    
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *topSearchView;
@property (weak, nonatomic) IBOutlet UIView *topStatusBarView;
@property (weak, nonatomic) IBOutlet UIView *underSearchBarView;

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSArray *filteredUsers;
@property (strong, nonatomic) NSMutableArray *displayedItems;

@end

@implementation MWSearchViewController

#pragma mark - Controller Life Cycle
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _users = [self stalkers];
    
    self.filteredUsers = [NSMutableArray array];
    self.displayedItems = [NSMutableArray array];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:stalkerTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:stalkerTableViewCellID];
    [self.tableView registerNib:[UINib nibWithNibName:searchPlaceholderTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:searchPlaceholderTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureUI];
    [self configureSearchController];
}
    
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self performSelector:@selector(showKeyboard) withObject:nil afterDelay:0.1];
}

- (void)showKeyboard {
    [self.searchController.searchBar becomeFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
    
#pragma mark - Settings

- (void)configureUI {
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
    
    self.topStatusBarView.backgroundColor = UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f);
    self.underSearchBarView.backgroundColor = UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f);
}
    
- (void)configureSearchController {
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder = nil;
    [self.searchController.searchBar sizeToFit];
    
    self.searchController.searchBar.translucent = YES;
    [self.searchController.searchBar setSearchBarStyle:UISearchBarStyleDefault];
    
    self.searchController.searchBar.barTintColor = UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f);
    self.searchController.searchBar.tintColor = [UIColor whiteColor];
    self.searchController.searchBar.backgroundColor = UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f);
    self.searchController.searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
    
    UITextField *textField = [self.searchController.searchBar valueForKey:@"searchField"];
    textField.textColor = [UIColor whiteColor];
    textField.backgroundColor = UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f);
    
    [textField setLeftViewMode:UITextFieldViewModeNever];
    [textField setRightViewMode:UITextFieldViewModeNever];
    [textField setBorderStyle:UITextBorderStyleNone];
    textField.clearButtonMode = UITextFieldViewModeNever;
    textField.placeholder = @"Search";
    [textField setFont:[UIFont fontWithName:_gothamPro_font_light size:16]];
    
    [self.topSearchView addSubview:self.searchController.searchBar];
    
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
}
    
#pragma mark - UISearchBarDelegate
    
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}
    
#pragma mark - UISearchControllerDelegate
    
- (void)didPresentSearchController:(UISearchController *)searchController {
    [self.searchController.searchBar becomeFirstResponder];
}
    
#pragma mark - UISearchResultsUpdating
    
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchString = searchController.searchBar.text;
    
    if (![searchString isEqualToString:@""]) {
        self.filteredUsers = [NSArray array];
        self.displayedItems = [NSMutableArray array];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.full_name beginswith[c] %@", searchString];
    
        NSMutableArray *tempArray = [NSMutableArray array];
        
        for (NSDictionary *user in self.users)
            [tempArray addObject:[user objectForKey:@"user"]];
        
        self.filteredUsers = [tempArray filteredArrayUsingPredicate:predicate];
        
        for (IGUser *user in self.filteredUsers)
            [self.displayedItems addObject:@{@"user" : user}];
        
    } else {
        self.displayedItems = [NSMutableArray array];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.displayedItems.count == 0)
        return 100;
    else
        return 95;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.displayedItems.count == 0)
        return 1;
    else
        return self.displayedItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_displayedItems.count == 0) {
        
        MWSearchPlaceholderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchPlaceholderTableViewCellID];
        
        return cell;
        
    } else {
        MWStalkerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:stalkerTableViewCellID];
        
        cell.followingView.hidden = YES;
        cell.followingViewWidthConstraint.constant = 0;
        
        IGUser *user = [_displayedItems[indexPath.row] objectForKey:@"user"];
        [cell.userImageView setImageWithURL:[NSURL URLWithString:user.profile_picture]];
        cell.userNameLabel.text = user.full_name;
        cell.userNickNameLabel.text = user.username;
        
        return cell;
    }
}

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_displayedItems.count == 0) return;
    
    MWProfileStalkerViewController *vc = [MWProfileStalkerViewController new];
    vc.user = [_displayedItems[indexPath.row] objectForKey:@"user"];
    
    __weak typeof(&*self)weakSelf = self;
    
    [weakSelf dismissViewControllerAnimated:NO completion:^{
        UIViewController *topVC = [AppDelegate topMostController];
        
        if ([topVC isKindOfClass:[UINavigationController class]])
            [((UINavigationController *)topVC) pushViewController:vc animated:YES];
    }];
}

#pragma mark - Helpers

- (NSMutableArray *)stalkers {
    
    MainUser *mainUser = [MainUser mainUser];
  
    RLMArray *likes = mainUser.likes;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    for (IGLiker *liker in likes)
        dict[liker.user.Id] = @{@"user" : liker.user};
    
    return [[NSMutableArray alloc] initWithArray:[dict allValues]];
}

@end
