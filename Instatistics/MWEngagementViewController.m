//
//  MWEngagementViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWEngagementViewController.h"
#import "MWGradientHeaderTableViewCell.h"
#import "MWSearchViewController.h"
#import "MWSettingsTableViewCell.h"
#import "MWUpgradeViewController.h"
#import "MWMostLikedViewController.h"
#import "MWStalkersViewController.h"

static NSString *gradientHeaderTableViewCellID = @"MWGradientHeaderTableViewCell";
static NSString *settingsTableViewCellID = @"MWSettingsTableViewCell";

@interface MWEngagementViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MWEngagementViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:gradientHeaderTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:gradientHeaderTableViewCellID];
     [self.tableView registerNib:[UINib nibWithNibName:settingsTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:settingsTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Engagement";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
    
    UIImage *searchImage = [UIImage imageNamed:@"Icon - Search"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    searchButton.bounds = CGRectMake(0, 0, 24, 24);
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    UIBarButtonItem *searchBI = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    [searchButton addTarget:self action:@selector(onSearchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = searchBI;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 8)
        return;
    
    BOOL showLockedItems = [AppUtils showLockedItems];
    
    if (showLockedItems) {
        MWUpgradeViewController *vc = [MWUpgradeViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        
        switch (indexPath.row) {
            case 1: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = MostLikesToMeScreenType;
                [self.navigationController pushViewController:vc animated:YES];
                
            } break;
                
            case 2: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = MostCommentsScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 3: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = MostCommentsAndLikesScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
            
            /* ---------------------------- */
                
            case 5: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = LeastLikesGivenScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 6: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = LeastCommentsLeftScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 7: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = NoCommentsOrLikesScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
              
            /* ---------------------------- */
                
            case 9: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = MyRecentFavouriteUsersScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 10: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = MyBestFriendsScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 11: {
                MWStalkersViewController *vc = [MWStalkersViewController new];
                vc.stalkersScreenType = UsersILikeButDontFollowScreenType;
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            default: break;
        }
    }
}

- (void)onSearchButtonPressed:(id)sender {
    [self presentViewController:[MWSearchViewController new] animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_medal"] Title:@"My Best Followers"];
            return cell;
        } break;
            
        case 1: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Most Likes to Me";
            return cell;
        } break;
            
        case 2: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Most Comments to Me";
            return cell;
        } break;
            
        case 3: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Most Comments and Likes";
            return cell;
        } break;
          
            /* ------------------------------- */
            
        case 4: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_padlock"] Title:@"Ghost Followers"];
            return cell;
        } break;
            
        case 5: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Least Likes Given";
            return cell;
        } break;
            
        case 6: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Least Comments Left";
            return cell;
        } break;
            
        case 7: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"No comments or Likes";
            return cell;
        } break;
            
            /* ------------------------------- */
            
        case 8: {
            MWGradientHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gradientHeaderTableViewCellID];
            [cell configureWithIcon:[UIImage imageNamed:@"s_clock"] Title:@"Discover Followers"];
            return cell;
        } break;
        
        case 9: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"My Recent Favourite Users";
            return cell;
        } break;
            
        case 10: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"My Best Friends";
            return cell;
        } break;
            
        case 11: {
            MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
            cell.mainLabel.text = @"Users I Like But Don't Follow";
            return cell;
        } break;
            
        default: break;
    }
    
    MWSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsTableViewCellID];
    
    return cell;
}

@end
