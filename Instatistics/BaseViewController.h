//
//  BaseViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppConstants.h"
#import "AppUtils.h"
#import "IGUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LeftMenuView.h"
#import "CloseCrossButtonCell.h"
#import "InstagramAPI.h"
#import "MultiLineStringCell.h"
#import "SeparatorPaddingCell.h"
#import "UIButton+InfoObject.h"
#import "MainUser.h"


@interface BaseViewController: UIViewController

@property (nonatomic,retain) MainUser *mainUser;

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) BOOL statusBarHidden;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet UIView *rightCancelView;
@property (weak, nonatomic) IBOutlet UIButton *extraButton;


- (void)logout:(MainUser *)mainUser;
- (BaseViewController *)startPage;

- (IBAction)buyItem:(UIButton *)sender;
- (IBAction)openUpgradeViewController:(UIButton *)sender;

@end
