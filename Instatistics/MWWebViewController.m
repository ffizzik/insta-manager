//
//  MWWebViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 11/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWWebViewController.h"

@interface MWWebViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation MWWebViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;
    self.webView.allowsInlineMediaPlayback = YES;
    
    switch (_screenType) {
        case TermsOfUseWebScreenType: {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", @"http://www.vapejet.com/followers-privacy-policy.html"]]]];
        } break;
        
        case AboutSubscriptionsWebScreenType: {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", @"http://www.netstick.com"]]]];
        } break;
            
        case FollowInstagramWebScreenType: {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", @"http://"]]]];
        } break;
            
        default: break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
}

#pragma mark - Settings

- (void)configureNavigationBar {
    
    self.navigationController.navigationBarHidden = NO;
    
    switch (_screenType) {
        case TermsOfUseWebScreenType: {
            self.navigationItem.title = @"Terms of Use";
        } break;
            
        case AboutSubscriptionsWebScreenType: {
            self.navigationItem.title = @"About Subscriptions";
        } break;
            
        case FollowInstagramWebScreenType: {
            self.navigationItem.title = @"Follow us on Instagram";
        } break;
            
        default: break;
    }
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    UIBarButtonItem *closeBI = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(onCloseButtonPressed:)];
    self.navigationItem.leftBarButtonItem = closeBI;
}

#pragma mark - Actions

- (void)onCloseButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {}

- (void)webViewDidFinishLoad:(UIWebView *)webView {}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {}

@end
