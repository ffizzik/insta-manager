//
//  UserStatisticsHorisontalInfoCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "UserStatisticsHorisontalInfoCell.h"
#import "HorisontalMenuCell.h"
#import "HorisontalMenuItem.h"
#import "HorisontalMenuItemView.h"
#import "UserStatisticsViewController.h"

@implementation UserStatisticsHorisontalInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_PURPLE, 0.5f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_PURPLE, 0.5f) Frame:imageView.bounds];
    [self insertSubview:imageView atIndex:0];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"HorisontalMenuCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HorisontalMenuCell"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    if (_isRunning == NO) {
        [self performSelector:@selector(setBandToMiddlePosition) withObject:nil afterDelay:0.1];
    }
}

- (void)changeBandPosition {
    
    if (self.collectionView.isDecelerating == NO && _isTracking == NO) {
        [self.collectionView setContentOffset:CGPointMake(_collectionView.contentOffset.x + 0.5, 0) animated:NO];
        
    }
    if (_collectionView.contentOffset.x  >= _collectionView.contentSize.width - self.frame.size.width) {
        [_collectionView setContentOffset:CGPointMake(_collectionView.contentSize.width/2.0f, 0) animated:NO];
    }
    _isRunning = YES;
   

    double delayInSeconds = 0.002;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self changeBandPosition];
    });
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _isTracking = YES;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _isTracking = NO;
}

- (void)setBandToMiddlePosition {
    [_collectionView setContentOffset:CGPointMake(_collectionView.contentSize.width/2.0f, 0) animated:NO];
    [self changeBandPosition];
}

#pragma mark <UICollectionViewDataSource>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake([AppUtils widthForStatisticsMenuItem:_itemsArray[indexPath.row % 8]], 60);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _itemsArray.count * 100;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HorisontalMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HorisontalMenuCell" forIndexPath:indexPath];
    [cell setupWithItem:_itemsArray[indexPath.row % 8]];
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end

