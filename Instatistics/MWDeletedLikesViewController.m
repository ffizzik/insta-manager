//
//  MWDeletedLikesViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 02/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWDeletedLikesViewController.h"
#import "MWDeletedLikeTableViewCell.h"
#import "MWTextPlaceholderTableViewCell.h"

#import "MainUser.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+NVTimeAgo.h"

static NSString *deletedLikeTableViewCellID = @"MWDeletedLikeTableViewCell";
static NSString *textPlaceholderTableViewCellID = @"MWTextPlaceholderTableViewCell";

@interface MWDeletedLikesViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *media;

@end

@implementation MWDeletedLikesViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _media = [NSMutableArray array];
    
    switch (_screenType) {
        case MWDeletedPhotosContentScreenType: {
            
        } break;
        
        case MWDeletedLikesContentScreenType: {
            _media = [self getMediaArray];
        } break;
        
        default: break;
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:deletedLikeTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:deletedLikeTableViewCellID];
    [self.tableView registerNib:[UINib nibWithNibName:textPlaceholderTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:textPlaceholderTableViewCellID];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    switch (_screenType) {
        case MWDeletedPhotosContentScreenType: {
            self.navigationItem.title = @"Deleted";
        } break;
        
        case MWDeletedLikesContentScreenType: {
            self.navigationItem.title = @"Deleted";
        } break;
        
        default: break;
    }
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_media.count == 0) return self.view.frame.size.height - 20;
    else                   return 90;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_media.count == 0) return 1;
    else                   return _media.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_media.count == 0) {
        MWTextPlaceholderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:textPlaceholderTableViewCellID];
        return cell;
    }
    
    MWDeletedLikeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deletedLikeTableViewCellID];
   
    IGLiker *liker = [_media objectAtIndex:indexPath.row];
    
    if (liker.user) [cell.userImageView setImageWithURL:[NSURL URLWithString:liker.user.profile_picture]];
    if (liker.liked_picture) [cell.photoImageView setImageWithURL:[NSURL URLWithString:liker.liked_picture]];
    
    if (liker.created_date) cell.timeLabel.text = [liker.created_date formattedAsTimeAgo]; //[DateHelpers getStringFromDate:liker.created_date];
    else                    cell.timeLabel.text = @"";
        
    cell.userNameLabel.text = liker.user.full_name;
    
    return cell;
}
    
#pragma mark - Database Calculations

- (NSMutableArray *)getMediaArray {

    MainUser *mainUser = [MainUser mainUser];
    
    switch (_screenType) {
        case MWDeletedLikesContentScreenType: {
            
            RLMArray *likes = mainUser.deletedLikes;
            NSMutableArray *unlikedPhoto = [NSMutableArray new];
            
            for (IGLiker *liker in likes)
                [unlikedPhoto addObject:liker];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_date"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [unlikedPhoto sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];;
            
        } break;
        
        case MWDeletedPhotosContentScreenType: {
            
            
            return [[NSMutableArray alloc] init];
        }
        
        default: break;
    }
}
    
@end
