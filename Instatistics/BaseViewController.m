//
//  BaseViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import <StoreKit/StoreKit.h>
#import "MidnightRate.h"
#import "DataLoader.h"

@interface BaseViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver>


@end


@implementation BaseViewController {
    
    NSString *selectedProduct;
    NSString *paidProduct;
}

#pragma mark - Controller Life Cycle

- (instancetype)init {
    self = [super init];
    if (self) {
        _mainUser = [MainUser mainUser];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _mainUser = [MainUser mainUser];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = [AppUtils appBackgroundColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRestoreButtonPressedNotification:) name:@"restoreButtonPressedNotification" object:nil];
    
    CGRect frame = self.navigationController.view.frame; frame.origin.y = 0;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.navigationController.view insertSubview:imageView atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.view endEditing:YES];
    [self removeObservers];
}

#pragma mark - UI configurations

- (void)removeObservers {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)prefersStatusBarHidden {
    return (self.statusBarHidden) ? YES : NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Notifications

- (void)onRestoreButtonPressedNotification:(NSNotification *)notification {
    [self restore];
}

#pragma mark - Actions

- (IBAction)openUpgradeViewController:(UIButton *)sender {
    [self.navigationController performSegueWithIdentifier:segue_showUpgradeView sender:nil];
}

- (void)logout:(MainUser *)mainUser {
    
    [MainUser removeUser:mainUser];
    //_usersToSwitch = [RegisteredUser allUsers];
    
    //if (_usersToSwitch.count == 0) {
    [[InstagramAPI sharedInstance] logout];
    [self.navigationController popViewControllerAnimated:YES];
    [[self startPage] performSegueWithIdentifier:segue_showAccountState sender:@(_not_connected)];
    //    return;
    //}
    
    //    RegisteredUser *user = _usersToSwitch[0];
    //    self.mainUser = user.currentUser;
    //
    //    [self setTitleUpImage:YES];
}

- (BaseViewController *)startPage {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    StartPageViewController *startPage = navigationController.startViewController;
    startPage.mainUser = [[InstagramAPI sharedInstance] loggedInUser];
    return startPage;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string{
    
    if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) {
        return NO;
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
        return NO;
    }
    return YES;
}

#pragma mark - Payment Actions

- (IBAction)buyItem:(UIButton *)sender {
    
    if      (sender.tag == 1)   selectedProduct = UPGRADE_ID_3;
    else if (sender.tag == 2)   selectedProduct = UPGRADE_ID_2;
    else if (sender.tag == 3)   selectedProduct = UPGRADE_ID_1;
    
    /*NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    [Storage setExpirationDate:newDate];
    [self dismissViewControllerAnimated:YES completion:nil];*/
    
    if([SKPaymentQueue canMakePayments]) {
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:selectedProduct]];
        productsRequest.delegate = self;
        [productsRequest start];
        
    } else {
        //this is called the user cannot make payments, most likely due to parental controls
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        request.delegate = self;
        [self purchase:validProduct];
        
    } else if(!validProduct) {
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [self paymentQueue:queue updatedTransactions:queue.transactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    
    if (transactions.count == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:@"Your subscriptions were expired or never purchased" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {}];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    for(SKPaymentTransaction *transaction in transactions) {
        
        switch(transaction.transactionState) {
                
            case SKPaymentTransactionStateDeferred:
                break;
                
            case SKPaymentTransactionStatePurchasing:
                break;
                
            case SKPaymentTransactionStatePurchased: {
                paidProduct = selectedProduct;
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateRestored: {
                //paidProduct = transaction.originalTransaction.payment.productIdentifier;
                NSLog(@"Successfully restored");
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateFailed: {
                NSString *messageString = transaction.error.localizedDescription;
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:messageString preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Retry"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //[self buyItem:_tempSender];
                                           }];
                
                [alert addAction:okButton];

                [self presentViewController:alert animated:YES completion:nil];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
        }
    }
}

- (void)notifySubscribtion:(SKPaymentTransaction *)transaction {
    
    [Storage setSubsciptionsStatus:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[MidnightRate sharedInstance] purchased];
    }];
}

@end
