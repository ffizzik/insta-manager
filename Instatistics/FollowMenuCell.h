//
//  FollowMenuCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/30/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerticalMenuItem.h"


@interface FollowMenuCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *changingCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) id target;

- (void)setupWithItem:(VerticalMenuItem *)item;

@end
