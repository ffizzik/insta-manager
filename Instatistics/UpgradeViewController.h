//
//  UpgradeViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseTableViewController.h"


@interface UpgradeViewController: BaseTableViewController

@property (nonatomic,assign) NSInteger selectedTag;

- (void)buildInterface;

@end
