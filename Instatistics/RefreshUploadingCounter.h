//
//  RefreshUploadingCounter.h
//  Instatistics
//
//  Created by . on 24/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface RefreshUploadingCounter : NSObject

@property (assign, nonatomic, readonly) NSInteger dbRequestsCount;
@property (assign, nonatomic, readonly) NSInteger fetchRequestsCount;

- (instancetype)initWithDBRequestsCount:(NSInteger)dbRequestsCount FetchRequestsCount:(NSInteger)fetchRequestsCount;

- (void)setPercents:(CGFloat)percents ForDBRequestIndex:(NSInteger)index;
- (void)setPercents:(CGFloat)percents ForFetchRequestIndex:(NSInteger)index;

- (void)clear;

- (NSInteger)getTotalPercent;

@end
