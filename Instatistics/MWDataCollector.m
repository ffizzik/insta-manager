//
//  MWDataCollector.m
//  Instatistics
//
//  Created by Denis Svichkarev on 30/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWDataCollector.h"
#import "MainUser.h"
#import "HorisontalMenuItem.h"
#import "VerticalMenuItem.h"

@implementation MWDataCollector

+ (NSMutableArray *)getItemsArrayForHorizontalCell {
    
    NSMutableArray *result = [NSMutableArray array];
    
    MainUser * previousUser = [MainUser previousUser];
    float previousPostsCount = 0;
    for (IGMedia *media in previousUser.photos) {
        NSTimeInterval aWeekAgo = [[NSDate date] timeIntervalSince1970] - 7 * 24 * 60 * 60;
        previousPostsCount += aWeekAgo < [media.created_time longLongValue] ? 1 : 0;
    }
    
    for (IGMedia *media in previousUser.videos) {
        NSTimeInterval aWeekAgo = [[NSDate date] timeIntervalSince1970] - 7 * 24 * 60 * 60;
        previousPostsCount += aWeekAgo < [media.created_time longLongValue] ? 1 : 0;
    }
    
    MainUser * currentUser = [MainUser mainUser];
    float currentPostsCount = 0;
    for (IGMedia *media in currentUser.photos) {
        NSTimeInterval aWeekAgo = [[NSDate date] timeIntervalSince1970] - 7 * 24 * 60 * 60;
        currentPostsCount += aWeekAgo < [media.created_time longLongValue] ? 1 : 0;
    }
    
    for (IGMedia *media in currentUser.videos) {
        NSTimeInterval aWeekAgo = [[NSDate date] timeIntervalSince1970] - 7 * 24 * 60 * 60;
        currentPostsCount += aWeekAgo < [media.created_time longLongValue] ? 1 : 0;
    }
    
    float currentMediaCount = currentUser.videos.count + currentUser.photos.count;
    float previousMediaCount = previousUser.videos.count + previousUser.photos.count;
    
    NSArray *infoArray = @[@{
                               @"title" : @"followers",
                               @"currentCount" : @(currentUser.userFollowers.count),
                               @"previousCount" : @(previousUser.userFollowers.count),
                               @"isFloat" : @(NO)
                               },
                           
                           @{
                               @"title" : @"following",
                               @"currentCount" : @(currentUser.userFollowings.count),
                               @"previousCount" : @(previousUser.userFollowings.count),
                               @"isFloat" : @(NO)
                               },
                           
                           @{
                               @"title" : @"photos",
                               @"currentCount" : @(currentUser.photos.count),
                               @"previousCount" :@(previousUser.photos.count),
                               @"isFloat" : @(NO)
                               },
                           @{
                               @"title" : @"videos",
                               @"currentCount" : @(currentUser.videos.count),
                               @"previousCount" : @(previousUser.videos.count),
                               @"isFloat" : @(NO)
                               },
                           @{
                               @"title" : @"likes total",
                               @"currentCount" : @(currentUser.likes.count),
                               @"previousCount" : @(previousUser.likes.count),
                               @"isFloat" : @(NO)
                               },
                           @{
                               @"title" : @"comments total",
                               @"currentCount" : @(currentUser.comments.count),
                               @"previousCount" : @(previousUser.comments.count),
                               @"isFloat" : @(NO)
                               },
                           @{
                               @"title" : @"posts per week",
                               @"currentCount" : @(currentPostsCount),
                               @"previousCount" : @(previousPostsCount),
                               @"isFloat" : @(NO)
                               },
                           @{
                               @"title" : @"comments per photo",
                               @"currentCount" : @(currentMediaCount == 0 ? 0 : (float)currentUser.comments.count / currentMediaCount),
                               @"previousCount" : @(previousMediaCount == 0 ? 0 : (float)previousUser.comments.count / previousMediaCount),
                               @"isFloat" : @(YES)
                               },
                           @{
                               @"title" : @"likes per photo",
                               @"currentCount" : @(currentMediaCount == 0 ? 0 : (float)currentUser.likes.count / currentMediaCount),
                               @"previousCount" : @(previousMediaCount == 0 ? 0 : (float)previousUser.likes.count / previousMediaCount),
                               @"isFloat" : @(YES)
                               }
                           ];
    
    for (NSDictionary *info in infoArray)
        [result addObject:[[HorisontalMenuItem new] initWithDictionary:info]];
    
    return result;
}

+ (NSMutableArray *)getItemsArrayForStatisticsCells {
    
    NSMutableArray *result = [NSMutableArray array];
    
    MainUser *currentUser = [MainUser mainUser];
    MainUser *previousUser = [MainUser previousUser];
    MainUser *storedUser = [MainUser storedUser];
    
    RLMArray *likes = currentUser.likes;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    for (IGLiker *liker in likes)
        dict[liker.user.Id] = @{@"user" : liker.user};
    
    NSMutableArray *profileStalkers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
    
    RLMArray *oldLikes = previousUser.likes;
    dict = [NSMutableDictionary new];
    
    for (IGLiker *liker in oldLikes)
        dict[liker.user.Id] = @{@"user" : liker.user};
    
    NSMutableArray *oldProfileStalkers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
    
    NSArray * iconsArray = @[@{@"icon" : @"ms_followers_gained",
                               @"title" : @"FOLLOWERS GAINED",
                               @"currentCount" : @(currentUser.userNewFollowers.count),
                               @"previousCount" : @(previousUser.userNewFollowers.count),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_followers_lost",
                               @"title" : @"FOLLOWERS LOST",
                               @"currentCount" : @(currentUser.userLostFollowers.count),
                               @"previousCount" : @(previousUser.userLostFollowers.count),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_i_dont_follow_back",
                               @"title" : @"I DON'T FOLLOW BACK",
                               @"currentCount" : @(currentUser.userIAmNotFollowingBack.count),
                               @"previousCount" : @(previousUser.userIAmNotFollowingBack.count),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_not_following",
                               @"title" : @"NOT FOLLOWING ME",
                               @"currentCount" : @(currentUser.usersNotFollowMeBack.count),
                               @"previousCount" : @(previousUser.usersNotFollowMeBack.count),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_blocking_me",
                               @"title" : @"BLOCKING ME",
                               @"currentCount" : @(0),
                               @"previousCount" : @(0),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_deleted_likes",
                               @"title" : @"DELETED LIKES",
                               @"currentCount" : @(currentUser.deletedLikes.count),
                               @"previousCount" : @(0),
                               @"drawBottomSeparator" : @(true)
                               },
                             @{@"icon" : @"ms_photos",
                               @"title" : @"DELETED TAGGED PHOTOS",
                               @"currentCount" : @(0),
                               @"previousCount" : @(0),
                               @"drawBottomSeparator" : @(false)
                               },
                             @{@"icon" : @"ms_visitors",
                               @"title" : @"NEW PROFILE STALKERS",
                               @"currentCount" : @(profileStalkers.count),
                               @"previousCount" : @(oldProfileStalkers.count),
                               @"drawBottomSeparator" : @(false)
                               }
                             ];
    
    for (NSDictionary *dict in iconsArray) {
        VerticalMenuItem *item = [[VerticalMenuItem new] initWithDictionary:dict];
        [result addObject:item];
    }
    
    return result;
}

@end
