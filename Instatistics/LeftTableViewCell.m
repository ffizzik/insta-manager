//
//  LeftTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 26/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "LeftTableViewCell.h"

@implementation LeftTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.leftDarkView.backgroundColor = UIColorFromRGBA(COLOR_RIGHT_GRADIENT_PURPLE, 1.f);
        self.leftWhiteView.hidden = NO;
        self.titleLabel.textColor = [UIColor whiteColor];
        
    } else {
        self.leftDarkView.backgroundColor = [UIColor clearColor];
        self.leftWhiteView.hidden = YES;
        self.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
    }
}

@end
