//
//  PhotoItemCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/30/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoItemCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic,strong) id target;

- (void)setupWithItem:(NSString *)imageUrl;

@end
