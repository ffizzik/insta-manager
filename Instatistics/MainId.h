//
//  MainId.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 12/13/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <Realm/Realm.h>


@interface MainId : RLMObject
@property NSString *mainId;
+(NSString*)mainId;
+(void)setMainId:(NSString*)mainId;
@end
