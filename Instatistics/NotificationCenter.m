//
//  NotificationCenter.m
//  Facetate
//
//  Created by Denis Svichkarev on 9/04/2017.
//  Copyright © 2017 Denis Svichkarev. All rights reserved.
//

#import "NotificationCenter.h"
#import "PDKeychainBindings.h"

NSString * const kReminderNotificationRequest   = @"kReminderNotificationRequest";
NSString * const kReminderNotification          = @"kReminderNotification";

NSString * const kTitle                         = @"Profile Visitors";
NSString * const kMessage                       = @"You have NEW Profile Visitors!";

@import UserNotifications;


@implementation NotificationCenter

static NotificationCenter *sharedNotificationCenter = nil;

#pragma mark - Settings

+ (instancetype)sharedNotificationCenter {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) [self setup];
    return self;
}

- (void)setup {
    self.isGrantedNotificationAccess = NO;
}

- (void)requestAuthorization {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                          completionHandler:^(BOOL granted, NSError *error) {
                              if (!error && granted) {
                                  self.isGrantedNotificationAccess = YES;
                              }
                          }];
}

- (void)isPendingNotificatonRequests:(void(^)(BOOL response))completion {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> *requests) {
        if (requests && requests.count > 0)
            completion(YES);
        else
            completion(NO);
    }];
}

- (BOOL)notificationsInstalled {
    if ([[PDKeychainBindings sharedKeychainBindings] objectForKey:kReminderNotification])
        return YES;
    else {
        return NO;
    }
}

#pragma mark - Remind notifications

- (void)setNotificationsWithTimeInterval:(NSTimeInterval)timeInterval Repeat:(BOOL)repeat {
    
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:kTitle arguments:nil];
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:kMessage
                                                                        arguments:nil];
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    
    UNTimeIntervalNotificationTrigger *trigger;
    
    trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:timeInterval repeats:repeat];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:kReminderNotificationRequest
                                                                          content:objNotificationContent
                                                                          trigger:trigger];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError *error) {
        if (!error) {
            [[PDKeychainBindings sharedKeychainBindings] setObject:kReminderNotification forKey:kReminderNotification];
            NSLog(@"Local Notification succeeded");
        }
        else {
            NSLog(@"Local Notification failed");
        }
    }];
}

- (void)removeRemindNotifications {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center removePendingNotificationRequestsWithIdentifiers:@[kReminderNotificationRequest]];
    
    if ([[PDKeychainBindings sharedKeychainBindings] objectForKey:kReminderNotification])
        [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:kReminderNotification];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

@end
