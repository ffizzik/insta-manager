//
//  MWDataCollector.h
//  Instatistics
//
//  Created by Denis Svichkarev on 30/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MWDataCollector : NSObject

+ (NSMutableArray *)getItemsArrayForHorizontalCell;

+ (NSMutableArray *)getItemsArrayForStatisticsCells;

@end
