//
//  BaseTableViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/5/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseViewController.h"
#import <ImoDynamicTableView/ImoDynamicTableView.h>
#import "SeparatorCell.h"
#import "SpaceCell.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import "UserStatisticsHorisontalInfoCell.h"

@interface BaseTableViewController : BaseViewController

@property (nonatomic, strong) UIRefreshControl *topRefreshControl;
@property (nonatomic, strong) UIRefreshControl *bottomRefreshControl;

@property (nonatomic, strong) IGPagination *pagination;
@property (strong, nonatomic) UserStatisticsHorisontalInfoCell *horizontalStaticCell;

@end
