//
//  MWExtraTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MWExtraTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *roundLeftView;
@property (weak, nonatomic) IBOutlet UIView *roundLeftWhiteView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end
