//
//  MWProfileStalkerViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 30/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWProfileStalkerViewController.h"
#import "UserProfileHeaderCell.h"
#import "UserPhotosCell.h"

#import "AppUtils.h"
#import "InstagramAPI.h"
#import "UIImageView+AFNetworking.h"

typedef NS_ENUM(NSUInteger, UserPhotoType) {
    GridUserPhotoType = 0,
    LikesPhotoType
};

static NSString *userProfileHeaderCellID = @"UserProfileHeaderCell";
static NSString *userPhotosCellID = @"UserPhotosCell";

@interface MWProfileStalkerViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (assign, nonatomic) CGFloat photosCellHeight;
@property (assign, nonatomic) CGFloat photosLikesCellHeight;

@property (assign, nonatomic) UserPhotoType userPhotoType;

@end

@implementation MWProfileStalkerViewController {

    NSString *nextMaxId;
    BOOL loading;
    NSMutableArray *itemsArray;
    NSMutableArray *likesItemsArray;
    UIActivityIndicatorView *bottomRefreshControl;
}

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nextMaxId = @"";
    loading = NO;
    itemsArray = [NSMutableArray array];
    likesItemsArray = [NSMutableArray array];
    
    _photosCellHeight = 0;
    _photosLikesCellHeight = 0;
    _userPhotoType = GridUserPhotoType;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.estimatedRowHeight = 250;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.tableView registerNib:[UINib nibWithNibName:userProfileHeaderCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:userProfileHeaderCellID];
    [self.tableView registerNib:[UINib nibWithNibName:userPhotosCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:userPhotosCellID];
    
    [self getUser];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = _user.full_name;
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getLikedPhotos];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return UITableViewAutomaticDimension;
    
    } else {
        if (_userPhotoType == GridUserPhotoType)
            return _photosCellHeight;
        else
            return _photosLikesCellHeight;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        UserProfileHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:userProfileHeaderCellID];
        
        [cell.userImageView setImageWithURL:[NSURL URLWithString:_user.profile_picture]];
        
        cell.postsCountLabel.text = [AppUtils minimizedCountString:[_user.media_count integerValue]];
        cell.followerCountLabel.text = [AppUtils minimizedCountString:[_user.followed_by_count integerValue]];
        cell.followingCountLabel.text = [AppUtils minimizedCountString:[_user.follows_count integerValue]];
        cell.userBioLabel.text = _user.bio;
        
        if (_userPhotoType == GridUserPhotoType) {
            [cell.gridImageView setImage:[UIImage imageNamed:@"ps_grid_blue"]];
            [cell.likesImageView setImage:[UIImage imageNamed:@"ps_likes_white"]];
            
            cell.gridBottomView.hidden = NO;
            cell.likesBottomView.hidden = YES;
            
        } else {
            [cell.gridImageView setImage:[UIImage imageNamed:@"ps_grid_white"]];
            [cell.likesImageView setImage:[UIImage imageNamed:@"ps_likes_blue"]];
            
            cell.gridBottomView.hidden = YES;
            cell.likesBottomView.hidden = NO;
        }
        
        [cell.gridButton addTarget:self action:@selector(onGridButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.likesButton addTarget:self action:@selector(onLikesButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    } else {
        
        UserPhotosCell *cell = [tableView dequeueReusableCellWithIdentifier:userPhotosCellID];
        cell.user = _user;
        
        if (_userPhotoType == GridUserPhotoType)
            cell.itemsArray = itemsArray;
        else
            cell.itemsArray = likesItemsArray;
        
        [cell.collectionView reloadData];
        
        return cell;
    }
}

#pragma mark - Actions

- (void)onGridButtonPressed:(id)sender {
    
    if (_userPhotoType == LikesPhotoType) {
        _userPhotoType = GridUserPhotoType;
        
        
        [self.tableView reloadData];
    }
}

- (void)onLikesButtonPressed:(id)sender {
    
    if (_userPhotoType == GridUserPhotoType) {
        _userPhotoType = LikesPhotoType;
        
        
        [self.tableView reloadData];
    }
}

#pragma mark - Refreshing

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGPoint offset = scrollView.contentOffset;
    CGSize size = scrollView.contentSize;
    
    if (size.height < _photosCellHeight) return;
    
    if (offset.y > scrollView.contentSize.height - 500) {
        
        if (!loading && ![nextMaxId isEqualToString:@""]) {
            loading = YES;
            
            if (!bottomRefreshControl)
                bottomRefreshControl = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 30, self.view.frame.size.width, 30)];
            if (![bottomRefreshControl isDescendantOfView:self.view])
                [self.view addSubview:bottomRefreshControl];
            
            bottomRefreshControl.hidden = NO;
            [bottomRefreshControl startAnimating];
            
            [self getPhotosForId:nextMaxId WithCompletion:^(BOOL success) {
                bottomRefreshControl.hidden = YES;
                [bottomRefreshControl stopAnimating];
                _photosCellHeight = ceil(itemsArray.count / 3.0) * ((self.view.frame.size.width - 6) / 3) + ceil(itemsArray.count / 3.0) * 3;
                [self.tableView reloadData];
                loading = NO;
            }];
        }
    }
}

- (void)getPhotosForId:(NSString *)nextId WithCompletion:(void (^)(BOOL success))completion {
    
    if (!_user) return;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [[InstagramAPI sharedInstance] getUserFeed:_user.Id count:0 maxId:nextId getComments:NO comp:^(NSArray *response, IGPagination *pagination) {
        nextMaxId = pagination.nextMaxId;
        [itemsArray addObjectsFromArray:response];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        completion(YES);
        
    } failure:^(NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        completion(NO);
    }];
}

- (void)getLikedPhotos {
    
    MainUser *mainUser = [MainUser mainUser];
    RLMArray *userPhotos = mainUser.photos;
    
    likesItemsArray = [NSMutableArray array];
    
    for (IGMedia *photo in userPhotos) {
        
        RLMArray *likes = photo.likes;
        
        for (IGLiker *like in likes) {
            if ([_user.Id isEqualToString:like.user.Id] && [photo.Id isEqualToString:like.mediaId] && [photo.type isEqualToString:@"photo"] && ![photo.image.standard_resolution isEqualToString:@""]) {
                [likesItemsArray addObject:photo];
                break;
            }
        }
    }
    
    _photosLikesCellHeight = ceil(likesItemsArray.count / 3.0) * ((self.view.frame.size.width - 6) / 3) + ceil(likesItemsArray.count / 3.0) * 3;
}

#pragma mark - Helpers

- (void)getUser {
    [[InstagramAPI sharedInstance] getUser:_user.Id comp:^(IGUser *user) {
        _user = user;
        
        if (!loading) {
            loading = YES;
            
            [self getPhotosForId:nextMaxId WithCompletion:^(BOOL success) {
                _photosCellHeight = ceil(itemsArray.count / 3.0) * ((self.view.frame.size.width - 6) / 3) + ceil(itemsArray.count / 3.0) * 3;
                
                [self.tableView reloadData];
                loading = NO;
            }];
        }

        [self.tableView reloadData];
    } failure:^(NSError *error) {}];
}

@end
