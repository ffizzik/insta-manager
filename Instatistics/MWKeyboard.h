//
//  MWKeyboard.h
//
//
//  Created by Denis Svichkarev on 02.03.16.
//
//

#import <Foundation/Foundation.h>

@interface MWKeyboard : NSObject

+ (MWKeyboard *)sharedInstance;

- (BOOL)isVisible;
- (CGFloat)height;
- (CGFloat)animationSpeed;

@end
