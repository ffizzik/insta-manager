//
//  MWKeyboard.m
//
//
//  Created by Denis Svichkarev on 02.03.16.
//
//

#import "MWKeyboard.h"

@interface MWKeyboard()

@property (assign, nonatomic) BOOL isVisible;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat animationSpeed;
@end

static MWKeyboard *_instance = nil;
static const CGFloat kInitialSpeed = 0.25;


@implementation MWKeyboard

+ (MWKeyboard *)sharedInstance {
    
    @synchronized(self) {
        if (!_instance) {
            _instance = [MWKeyboard new];
            _instance.animationSpeed = kInitialSpeed;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
        }
    }
    return _instance;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (void)keyboardWillChange:(NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    
    CGRect beginFrame = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect endFrame = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    _instance.height = beginFrame.size.height;
    _instance.isVisible = (beginFrame.origin.y >= endFrame.origin.y);
    _instance.animationSpeed = [info[UIKeyboardAnimationDurationUserInfoKey] floatValue];
}

@end
