//
//  MWMostLikedViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWMostLikedViewController.h"
#import "MWPhotoCollectionViewCell.h"

#import "MainUser.h"

@interface MWMostLikedViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation MWMostLikedViewController {
    NSMutableArray *photosItemsArray;
}

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"MWPhotoCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"MWPhotoCollectionViewCell"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;

    switch (_mediaScreenType) {
            
        case MostPopularMediaScreenType: {
            [self getPopularPhotos];
        } break;
        
        case MostLikedMediaScreenType: {
            [self getLikedPhotos];
        } break;
            
        case MostCommentedMediaScreenType: {
            [self getCommentedPhotos];
        } break;
            
        default: break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureUI];
}

- (void)configureUI {
    
    switch (_mediaScreenType) {
            
        case MostPopularMediaScreenType: {
            self.navigationItem.title = @"Most Popular";
        } break;
            
        case MostLikedMediaScreenType: {
            self.navigationItem.title = @"Most Liked";
        } break;
        
        case MostCommentedMediaScreenType: {
            self.navigationItem.title = @"Most Commented";
        } break;
            
        default: break;
    }
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark <UICollectionViewDataSource>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1, 0, 1, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = (collectionView.frame.size.width - 6) / 2.0f;
    return CGSizeMake(width, width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return photosItemsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MWPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MWPhotoCollectionViewCell" forIndexPath:indexPath];

    [cell setupWithItem:photosItemsArray[indexPath.row]];
    
    return cell;
}

- (void)getLikedPhotos {
    
    MainUser *mainUser = [MainUser mainUser];
    RLMArray *userPhotos = mainUser.photos;
    
    photosItemsArray = [NSMutableArray array];
    
    for (IGMedia *photo in userPhotos) {
        if ([photo.type isEqualToString:@"photo"] && ![photo.image.standard_resolution isEqualToString:@""]) {
            [photosItemsArray addObject:photo];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"likes_count"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [photosItemsArray sortedArrayUsingDescriptors:sortDescriptors];
    photosItemsArray = [NSMutableArray arrayWithArray:sortedArray];
    
    [_collectionView reloadData];
}

- (void)getCommentedPhotos {
    
    MainUser *mainUser = [MainUser mainUser];
    RLMArray *userPhotos = mainUser.photos;
    
    photosItemsArray = [NSMutableArray array];
    
    for (IGMedia *photo in userPhotos) {
        if ([photo.type isEqualToString:@"photo"] && ![photo.image.standard_resolution isEqualToString:@""]) {
            [photosItemsArray addObject:photo];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"comment_count"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [photosItemsArray sortedArrayUsingDescriptors:sortDescriptors];
    photosItemsArray = [NSMutableArray arrayWithArray:sortedArray];
    
    [_collectionView reloadData];
}

- (void)getPopularPhotos {
    
    MainUser *mainUser = [MainUser mainUser];
    RLMArray *userPhotos = mainUser.photos;
    
    photosItemsArray = [NSMutableArray array];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    for (IGMedia *photo in userPhotos) {
        if ([photo.type isEqualToString:@"photo"] && ![photo.image.standard_resolution isEqualToString:@""]) {
            NSInteger mediaCount = [photo.comment_count integerValue] + [photo.likes_count integerValue];
            dict[photo.Id] = @{@"photo" : photo, @"mediaCount" : @(mediaCount)};
        }
    }
    
    NSArray *mediaArray = [[NSMutableArray alloc] initWithArray:[dict allValues]];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mediaCount"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [mediaArray sortedArrayUsingDescriptors:sortDescriptors];
    mediaArray = [NSMutableArray arrayWithArray:sortedArray];
    
    for (int i = 0; i < mediaArray.count; i++) {
        [photosItemsArray addObject:[mediaArray[i] objectForKey:@"photo"]];
    }
    
    [_collectionView reloadData];
}

@end
