//
//  AppDelegate.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/28/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "AppDelegate.h"
#import "AppUtils.h"

#import "BaseNavigationController.h"
#import "LoginViewController.h"
//#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MidnightRate.h"
#import "AppConstants.h"
#import "IGUser.h"
#import "RegisteredUser.h"

#import "NotificationCenter.h"
#import "MWKeyboard.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self performRealmMigration];
    
    [AppUtils setApplicationAppearence];
//    [YMMYandexMetrica activateWithApiKey:YANDEX_ID];
    [FBSDKAppEvents activateApp];
    [MWKeyboard sharedInstance];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    //[[NotificationCenter sharedNotificationCenter] removeRemindNotifications];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BaseNavigationController *navController = [mystoryboard instantiateViewControllerWithIdentifier:@"BaseNavigationController"];

    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)performRealmMigration {
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = 3;
    
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
   
        if (oldSchemaVersion < 1) {

            [migration enumerateObjects:IGUser.className
                                  block:^(RLMObject *oldObject, RLMObject *newObject) {
                                      
                                      newObject[@"created_date"] = nil;
                                  }];
        }
        
        if (oldSchemaVersion < 2) {
            [migration enumerateObjects:RegisteredUser.className
                                  block:^(RLMObject *oldObject, RLMObject *newObject) {
                                      
                                      newObject[@"storedUser"] = nil;
                                  }];
        }
        
        if (oldSchemaVersion < 3) {
            [migration enumerateObjects:IGLiker.className
                                  block:^(RLMObject *oldObject, RLMObject *newObject) {
                                      
                                      newObject[@"created_date"] = nil;
                                      newObject[@"liked_picture"] = nil;
                                  }];
        }
    };
    
    [RLMRealmConfiguration setDefaultConfiguration:config];
    [RLMRealm defaultRealm];
}

+ (UIViewController *)topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

@end
