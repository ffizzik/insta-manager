//
//  MWUpgradeTopTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 04/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MWUpgradeTopTableViewCell : UITableViewCell

@property (assign, nonatomic) NSInteger selectedTag;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)configure;

@end
