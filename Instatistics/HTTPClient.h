//
//  HTTPClient.h
//  InstaTracker
//
//  Created by Midnight.Works iMac on 10/24/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "AFHTTPSessionManager.h"


@interface HTTPClient : AFHTTPSessionManager




+ (HTTPClient *)sharedHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (BOOL)connected;

@end
