//
//  IGLiker.h
//  Instamap
//
//  Created by Raul Andrisan on 6/26/11.
//  Copyright 2011 NextRoot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "IGLiker.h"
#import "IGUser.h"


@interface IGLiker : RLMObject <NSCoding> {
    
}

@property (nonatomic, retain) IGUser *user;
@property (nonatomic, retain) NSString *mediaId;

@property (nonatomic, retain) NSString *liked_picture;
@property (nonatomic, retain) NSDate *created_date;

+ (IGLiker *)likerWithDictionary:(NSDictionary *)dict;

@end

RLM_ARRAY_TYPE(IGLiker)
