//
//  UpgradeViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "UpgradeViewController.h"
#import "CloseCrossButtonCell.h"
#import "ImageViewCell.h"
#import "FolllowerPorCell.h"
#import "TabBarCell.h"
#import "BuyItemsCell.h"
#import "AppConstants.h"
#import "PageControllCell.h"


@interface UpgradeViewController ()

@end

@implementation UpgradeViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    [self buildInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Actions

- (IBAction)selectTag:(UIButton *)sender {
    _selectedTag = sender.tag;
    [self buildInterface];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)buildInterface {
    /*
    NSMutableArray *section = [NSMutableArray new];
    {
        SpaceCellSource *cellSource = [SpaceCellSource new];
        cellSource.staticHeightForCell = 15;
        [section addObject:cellSource];
    }
    {
        CloseCrossButtonCellSource *cellSource = [CloseCrossButtonCellSource new];
        cellSource.selector = @selector(closeView:);
        cellSource.staticHeightForCell = 25;
        cellSource.showRestoreButton = YES;
        [section addObject:cellSource];
    }
    {
        PageControllCellSource *cellSource = [PageControllCellSource new];
        cellSource.target = self;
        cellSource.selectedTag = _selectedTag;
        cellSource.staticHeightForCell = self.view.frame.size.height - 310;
        cellSource.backgroundColor = UIColorFromRGB(COLOR_DARK_PURPLE);
        [section addObject:cellSource];
    }
    {
        BuyItemsCellSource *cellSource = [BuyItemsCellSource new];
        cellSource.titleString = @"Subscribe for 12 months";
        cellSource.priceString = @"$1.99/mo";
        cellSource.bigPriceString = @"12";
        cellSource.cellTag = 1;
        cellSource.selector = @selector(buyItem:);
        cellSource.target = self;
        cellSource.staticHeightForCell = 80;
        cellSource.backgroundColor = UIColorFromRGB(COLOR_DARK_PURPLE);
        cellSource.mainViewColor = UIColorFromRGB(subscriptionOrangeBackgroundColor);
        
        [section addObject:cellSource];
    }
    {
        BuyItemsCellSource *cellSource = [BuyItemsCellSource new];
        cellSource.titleString = @"Subscribe for 6 months";
        cellSource.priceString = @"$2.99/mo";
        cellSource.bigPriceString = @"6";
        cellSource.cellTag = 2;
        cellSource.selector = @selector(buyItem:);
        cellSource.target = self;
        cellSource.staticHeightForCell = 80;
        cellSource.backgroundColor = UIColorFromRGB(COLOR_DARK_PURPLE);
        cellSource.mainViewColor = UIColorFromRGB(subscriptionYellowBackgroundColor);
        
        [section addObject:cellSource];
    }
    {
        BuyItemsCellSource *cellSource = [BuyItemsCellSource new];
        cellSource.titleString = @"Subscribe for 1 month";
        cellSource.priceString = @"$4.99/mo";
        cellSource.bigPriceString = @"1";
        cellSource.cellTag = 3;
        cellSource.selector = @selector(buyItem:);
        cellSource.target = self;
        cellSource.staticHeightForCell = 80;
        cellSource.backgroundColor = UIColorFromRGB(COLOR_DARK_PURPLE);
        cellSource.mainViewColor = UIColorFromRGB(subscriptionBlueBackgroundColor);
        
        [section addObject:cellSource];
    }
    
    [self.tableView.source removeAllObjects];
    [self.tableView.source addObject:section];
    [self.tableView reloadData];*/
}


@end
