//
//  HorisontalMenuCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/30/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorisontalMenuItem.h"

@interface HorisontalMenuCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

- (void)setupWithItem:(HorisontalMenuItem *)item;

@end
