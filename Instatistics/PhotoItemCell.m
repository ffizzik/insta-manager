//
//  PhotoItemCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/30/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "PhotoItemCell.h"
#import "AppUtils.h"
#import "AppConstants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IGMedia.h"

@implementation PhotoItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupWithItem:(IGMedia *)media {
    [_userImage sd_setImageWithURL:[NSURL URLWithString:media.image.standard_resolution]];
}

@end
