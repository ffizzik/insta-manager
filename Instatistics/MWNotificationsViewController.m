//
//  MWNotificationsViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 11/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWNotificationsViewController.h"
#import "PDKeychainBindings.h"
#import "NotificationCenter.h"

@interface MWNotificationsViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *profileStalkersSwitch;

@end

#pragma mark - Controller Life Cycle

@implementation MWNotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (![[PDKeychainBindings sharedKeychainBindings] objectForKey:@"MWNewProfileStalkersNotificationState"]) {
        [[PDKeychainBindings sharedKeychainBindings] setObject:@"on" forKey:@"MWNewProfileStalkersNotificationState"];
        [_profileStalkersSwitch setOn:YES];
    }
    
    if ([[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"MWNewProfileStalkersNotificationState"] isEqualToString:@"on"]) {
        [_profileStalkersSwitch setOn:YES];
    } else {
        [_profileStalkersSwitch setOn:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Notifications";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
    
    self.profileStalkersSwitch.onTintColor = UIColorFromRGB(COLOR_LIGHT_AZURE);
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (IBAction)onSwitchStateChanged:(id)sender {
    
    if (_profileStalkersSwitch.on) {
        [[PDKeychainBindings sharedKeychainBindings] setObject:@"on" forKey:@"MWNewProfileStalkersNotificationState"];
    } else {
        [[PDKeychainBindings sharedKeychainBindings] setObject:@"off" forKey:@"MWNewProfileStalkersNotificationState"];
        [[NotificationCenter sharedNotificationCenter] removeRemindNotifications];
    }
}

@end
