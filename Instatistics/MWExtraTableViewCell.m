//
//  MWExtraTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWExtraTableViewCell.h"

@implementation MWExtraTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.roundLeftView.layer.cornerRadius = self.roundLeftView.frame.size.height / 2;
    self.roundLeftWhiteView.layer.cornerRadius = self.roundLeftWhiteView.frame.size.height / 2;
    self.mainView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
