//
//  BaseTableViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/5/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseTableViewController.h"
#import "LeftTableViewCell.h"

#import "MWStalkersViewController.h"
#import "UserImageTableViewCell.h"
#import "UserStatisticsFollowInfoCell.h"
#import "MWDataCollector.h"
#import "MWUpgradeViewController.h"
#import "MWEngagementViewController.h"
#import "MWInsightPackViewController.h"
#import "MWSettingsViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface BaseTableViewController ()

@property (strong, nonatomic) NSArray *leftMenuTitles;

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _leftMenuTitles = @[@"PROFILE STALKERS", @"ENGAGEMENT PACK", @"INSIGHTS PACK", @"SETTINGS", @"LOG OUT"];
    
    self.leftTableView.allowsMultipleSelection = NO;
    [self.leftTableView registerNib:[UINib nibWithNibName:@"LeftTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LeftTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UserImageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UserImageTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserStatisticsHorisontalInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UserStatisticsHorisontalInfoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserStatisticsFollowInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UserStatisticsFollowInfoCell"];
    
    _horizontalStaticCell = [[NSBundle mainBundle] loadNibNamed:@"UserStatisticsHorisontalInfoCell" owner:self options:nil][0];
    _horizontalStaticCell.itemsArray = [MWDataCollector getItemsArrayForHorizontalCell];
    [_horizontalStaticCell.collectionView reloadData];
}

#pragma mark - UITableViewDelegate 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 100) {
        
        if (indexPath.row == 0) {
            
            UserImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserImageTableViewCell"];
            
            MainUser *mainUser = [MainUser mainUser];
            
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:mainUser.user.profile_picture]];
            cell.userNameLabel.text = mainUser.user.full_name;
            cell.nickNameLabel.text = [NSString stringWithFormat:@"@%@", mainUser.user.username];
            cell.followersCountLabel.text = [NSString stringWithFormat:@"%ld", mainUser.userFollowers.count];
            
            return cell;
            
        } else if (indexPath.row == 1) {
            
            return _horizontalStaticCell;
            
        } else  {
            
            UserStatisticsFollowInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserStatisticsFollowInfoCell"];
            
            cell.itemsArray = [MWDataCollector getItemsArrayForStatisticsCells];
            cell.target = self;
            [cell.collectionView reloadData];
            
            return cell;
        }
        
    } else {
        
        LeftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftTableViewCell"];
        
        cell.titleLabel.text = _leftMenuTitles[indexPath.row];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 100) {
        
        switch (indexPath.row) {
            case 0: return 150;
            case 1: return 60;
                
            default: return (self.view.frame.size.width / 2.0) * 0.7 * 4;;
        }
        
    } else
        return 64;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == 100)
        return 3;
    else
        return _leftMenuTitles.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 100) {
        
        
        
    } else {
        
        BOOL showLockedItems = [AppUtils showLockedItems];
        
        switch (indexPath.row) {
                
            case 0: {
                if (showLockedItems) {
                    MWUpgradeViewController *vc = [MWUpgradeViewController new];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                } else {
                    MWStalkersViewController *vc = [MWStalkersViewController new];
                    vc.stalkersScreenType = VisitorsStalkersScreenType;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            } break;
                
            case 1: {
                MWEngagementViewController *vc = [MWEngagementViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 2: {
                MWInsightPackViewController *vc = [MWInsightPackViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 3: {
                MWSettingsViewController *vc = [MWSettingsViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            } break;
                
            case 4: {
                [self logout:self.mainUser];
            } break;
                
            default: break;
        }
    }
}

@end
