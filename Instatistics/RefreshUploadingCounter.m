//
//  RefreshUploadingCounter.m
//  Instatistics
//
//  Created by . on 24/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "RefreshUploadingCounter.h"

@interface RefreshUploadingCounter()

@property (strong, nonatomic) NSMutableArray *dbRequests;
@property (strong, nonatomic) NSMutableArray *fetchRequests;

@end

@implementation RefreshUploadingCounter

- (instancetype)initWithDBRequestsCount:(NSInteger)dbRequestsCount FetchRequestsCount:(NSInteger)fetchRequestsCount {
    
    if (self = [super init]) {
        _dbRequestsCount = dbRequestsCount;
        _dbRequests = [NSMutableArray array];
        
        for (int i = 0; i < _dbRequestsCount; i++) {
            [_dbRequests addObject:@(0.)];
        }
        
        _fetchRequestsCount = fetchRequestsCount;
        _fetchRequests = [NSMutableArray array];
        
        for (int i = 0; i < _fetchRequestsCount; i++) {
            [_fetchRequests addObject:@(0.)];
        }
    }
    
    return self;
}

- (NSInteger)getTotalPercent {
    
    NSInteger fetchTotalSum = 0;
    
    for (int i = 0; i < _fetchRequestsCount; i++) {
        fetchTotalSum += [_fetchRequests[i] doubleValue];
    }
    
    if (_fetchRequests.count == 0) return 0;
    
    fetchTotalSum /= _fetchRequests.count * 2;
    
    NSInteger dbTotalSum = 0;
    
    for (int i = 0; i < _dbRequestsCount; i++) {
        dbTotalSum += [_dbRequests[i] doubleValue];
    }
    
    if (_dbRequests.count == 0) return 0;
    
    dbTotalSum /= _dbRequests.count * 2;
    
    return fetchTotalSum + dbTotalSum;
}

- (void)setPercents:(CGFloat)percents ForDBRequestIndex:(NSInteger)index {
   
    if (index - 1 > _dbRequestsCount) return;
    
    _dbRequests[index - 1] = @(percents);
}

- (void)setPercents:(CGFloat)percents ForFetchRequestIndex:(NSInteger)index {
    
    if (index - 1 > _fetchRequestsCount) return;
    
    _fetchRequests[index - 1] = @(percents);
}

- (void)clear {
    
    _dbRequests = [NSMutableArray array];
    
    for (int i = 0; i < _dbRequestsCount; i++) {
        [_dbRequests addObject:@(0.)];
    }
}

@end
