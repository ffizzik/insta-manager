//
//  UserStatisticsViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseTableViewController.h"


@interface UserStatisticsViewController: BaseTableViewController

@property (nonatomic, strong) RLMResults *usersToSwitch;
@property (nonatomic, assign) BOOL isVisible;

- (void)transitionToControllerWithIndex:(NSInteger)index;

@end
