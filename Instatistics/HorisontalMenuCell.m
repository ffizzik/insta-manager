//
//  HorisontalMenuCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/30/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "HorisontalMenuCell.h"
#import "AppUtils.h"
#import "AppConstants.h"

@implementation HorisontalMenuCell

- (void)setupWithItem:(HorisontalMenuItem *)item{

    self.backgroundColor = [UIColor clearColor];
    
    _countLabel.text = item.title;
    _titleLabel.text = [AppUtils minimizedCountString:(NSInteger)item.currentCount];
}

@end
