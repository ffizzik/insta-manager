//
//  StalkersCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "StalkersCell.h"
#import "MainUser.h"
#import "AppUtils.h"
#import "AppConstants.h"

@implementation StalkersCellSource

- (id)init {
  self = [super init];
  if (self) {
    self.cellClass = @"StalkersCell";
    self.backgroundColor = [UIColor clearColor];
    self.multipleSelection = YES;
  }
  return self;
}

@end


@implementation StalkersCell

- (NSArray *)stalkers {
    
    MainUser *mainUser = [MainUser mainUser];
    RLMArray *likes = mainUser.likes;
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    for (IGLiker *liker in likes) {
        dict[liker.user.Id] = @{@"user" : liker.user};
    }
    
    return [dict allValues];
}

- (void)setUpWithSource:(StalkersCellSource *)source {
    self.backgroundColor = [UIColor clearColor];
    self.target = source.target;
    _infoLabel.text = [NSString stringWithFormat:@"Profile Visitors %lu",(unsigned long)[self stalkers].count];
}

- (IBAction)selectItem:(UIButton*)sender {
    
    if ([AppUtils showLockedItems]) {
        UIViewController *viewController = [AppUtils topViewController];
        [viewController.navigationController performSegueWithIdentifier:segue_showUpgradeView sender:nil];
        return;
    }
    
    [_target performSegueWithIdentifier:segue_showUserAudienceTops sender:nil];
}


@end




