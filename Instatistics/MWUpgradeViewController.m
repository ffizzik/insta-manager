//
//  MWUpgradeViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 04/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWUpgradeViewController.h"
#import "MWUpgradeTopTableViewCell.h"
#import "BuyItemsCell.h"

#import <StoreKit/StoreKit.h>
#import "MidnightRate.h"
#import "DataLoader.h"
#import <SVProgressHUD/SVProgressHUD.h>


static NSString *buyItemsCellID = @"BuyItemsCell";
static NSString *upgradeTopTableViewCellID = @"MWUpgradeTopTableViewCell";

@interface MWUpgradeViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

#pragma mark - Controller Life Cycle

@implementation MWUpgradeViewController {
    NSString *selectedProduct;
    NSString *paidProduct;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:buyItemsCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:buyItemsCellID];
    [self.tableView registerNib:[UINib nibWithNibName:upgradeTopTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:upgradeTopTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Pro account";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStylePlain target:self action:@selector(onRestoreButtonPressed:)];
    
    UIImage *closeImage = [UIImage imageNamed:@"closeCrossButtonIcon"];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.bounds = CGRectMake(0, 0, 24, 24);
    [closeButton setImage:closeImage forState:UIControlStateNormal];
    UIBarButtonItem *closeBI = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    [closeButton addTarget:self action:@selector(onCloseButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = closeBI;
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObservers];
    self.navigationItem.title = @"";
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) return self.view.frame.size.height - (3 * self.view.frame.size.height * 0.15);
    else                    return self.view.frame.size.height * 0.15;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0: {
            MWUpgradeTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:upgradeTopTableViewCellID];
            
            [cell configure];
            
            return cell;
        } break;
            
        case 1: {
            BuyItemsCell *cell = [tableView dequeueReusableCellWithIdentifier:buyItemsCellID];
            
            cell.titleLabel.text = @"Subscribe for 12 months";
            cell.priceLabel.text = @"$1.99/mo";
            cell.bigPriceLabel.text = @"12";
            cell.mainView.backgroundColor = UIColorFromRGB(subscriptionOrangeBackgroundColor);
            
            cell.selectButton.tag = 0;
            [cell.selectButton addTarget:self action:@selector(onSubscription12MonthsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        } break;
            
        case 2: {
            BuyItemsCell *cell = [tableView dequeueReusableCellWithIdentifier:buyItemsCellID];
            
            cell.titleLabel.text = @"Subscribe for 6 months";
            cell.priceLabel.text = @"2.99/mo";
            cell.bigPriceLabel.text = @"6";
            cell.mainView.backgroundColor = UIColorFromRGB(subscriptionYellowBackgroundColor);
            
            cell.selectButton.tag = 1;
            [cell.selectButton addTarget:self action:@selector(onSubscription6MonthsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        } break;
            
        default: {
            BuyItemsCell *cell = [tableView dequeueReusableCellWithIdentifier:buyItemsCellID];
            
            cell.titleLabel.text = @"Subscribe for 1 month";
            cell.priceLabel.text = @"$4.99/mo";
            cell.bigPriceLabel.text = @"1";
            cell.mainView.backgroundColor = UIColorFromRGB(subscriptionBlueBackgroundColor);
            
            cell.selectButton.tag = 2;
            [cell.selectButton addTarget:self action:@selector(onSubscription1MonthButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        } break;
    }
}

#pragma mark - Payment Actions

- (void)removeObservers {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if (count > 0) {
        validProduct = [response.products objectAtIndex:0];
        request.delegate = self;
        [self purchase:validProduct];
        
    } else if (!validProduct) {
        [SVProgressHUD dismiss];
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [self paymentQueue:queue updatedTransactions:queue.transactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    
    [SVProgressHUD dismiss];
    
    if (transactions.count == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:@"Your subscriptions were expired or never purchased" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {}];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    for(SKPaymentTransaction *transaction in transactions) {
        
        switch(transaction.transactionState) {
                
            case SKPaymentTransactionStateDeferred:
                break;
                
            case SKPaymentTransactionStatePurchasing:
                break;
                
            case SKPaymentTransactionStatePurchased: {
                paidProduct = selectedProduct;
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateRestored: {
                //paidProduct = transaction.originalTransaction.payment.productIdentifier;
                NSLog(@"Successfully restored");
                [self notifySubscribtion:transaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
                
            case SKPaymentTransactionStateFailed: {
                NSString *messageString = transaction.error.localizedDescription;
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appErrorDomain message:messageString preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Retry"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //[self buyItem:_tempSender];
                                           }];
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            } break;
        }
    }
}

- (void)notifySubscribtion:(SKPaymentTransaction *)transaction {
    
    [Storage setSubsciptionsStatus:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[MidnightRate sharedInstance] purchased];
    }];
}

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)onRestoreButtonPressed:(id)sender {
    [SVProgressHUD show];
    [self restore];
}

- (void)onCloseButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onSubscription12MonthsButtonPressed:(id)sender {
    [SVProgressHUD show];
    selectedProduct = UPGRADE_ID_3;
    [self paymentRequest];
}

- (void)onSubscription6MonthsButtonPressed:(id)sender {
    [SVProgressHUD show];
    selectedProduct = UPGRADE_ID_2;
    [self paymentRequest];
}

- (void)onSubscription1MonthButtonPressed:(id)sender {
    [SVProgressHUD show];
    selectedProduct = UPGRADE_ID_1;
    [self paymentRequest];
}

- (void)paymentRequest {
    if([SKPaymentQueue canMakePayments]) {
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:selectedProduct]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
}

@end
