//
//  StartPageViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import "BaseViewController.h"

@interface StartPageViewController: BaseViewController

- (void)leftMenuItemSelected:(UIButton *)sender;

@end
