//
//  MWStalkersViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MWStalkersScreenType) {
    VisitorsStalkersScreenType = 0,
    NewFollowersStalkersScreenType,
    LostFollowersStalkersScreenType,
    DontFollowBackStalkersScreenType,
    NotFollowMeBackStalkersScreenType,
    BlockingMeStalkersScreenType,
    MostCommentsScreenType,
    MostCommentsAndLikesScreenType,
    LeastLikesGivenScreenType,
    LeastCommentsLeftScreenType,
    NoCommentsOrLikesScreenType,
    MyRecentFavouriteUsersScreenType,
    MyBestFriendsScreenType,
    UsersILikeButDontFollowScreenType,
    MostLikesToMeScreenType,
    EarliestFollowersScreenType,
    LatestFollowersScreenType,
    AllLostFollowersScreenType,
    UsersIUnfollowedScreenType
};

@interface MWStalkersViewController : UIViewController

@property (assign, nonatomic) MWStalkersScreenType stalkersScreenType;

@end
