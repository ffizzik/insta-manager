//
//  MWGradientHeaderTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MWGradientHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;

- (void)configureWithIcon:(UIImage *)icon Title:(NSString *)title;

@end
