//
//  MWStalkerTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWStalkerTableViewCell.h"

@implementation MWStalkerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height / 2;
    
    self.followsYouView.layer.cornerRadius = 5;
    self.youFollowView.layer.cornerRadius = 5;
    
    self.followsYouView.layer.borderWidth = 1;
    self.youFollowView.layer.borderWidth = 1;
    
    self.followsYouView.layer.borderColor = UIColorFromRGBA(followersGainedGreenColor, 1.f).CGColor;
    self.youFollowView.layer.borderColor = UIColorFromRGBA(followersGainedPurpleColor, 1.f).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.backgroundColor = UIColorFromRGB(stalkerSelectedGreenColor);
        self.selectedInnerView.backgroundColor = UIColorFromRGB(stalkerRedColor);
        
    } else {
        self.backgroundColor = [UIColor clearColor];
        self.selectedInnerView.backgroundColor = [UIColor clearColor];
    }
}

- (void)configureWithStalkersScreenType:(MWStalkersScreenType)screenType {
    
    _followingView.hidden = NO;
    _followingViewWidthConstraint.constant = 80;
    _selectedViewLeadingConstraint.constant = -24;
    _userNameTopConstraint.constant = 13;
    
    _selectedView.layer.cornerRadius = _selectedView.frame.size.height / 2;
    _selectedView.layer.borderColor = [UIColor whiteColor].CGColor;
    _selectedView.layer.borderWidth = 2.f;
    
    _selectedInnerView.layer.cornerRadius = _selectedInnerView.frame.size.height / 2;
    _selectedView.hidden = NO;
    _commentsLabel.hidden = NO;
    
    if (screenType == MyRecentFavouriteUsersScreenType || screenType == MyBestFriendsScreenType || screenType == NoCommentsOrLikesScreenType
        || screenType == EarliestFollowersScreenType || screenType == LatestFollowersScreenType || screenType == AllLostFollowersScreenType
        || screenType == UsersIUnfollowedScreenType || screenType == NewFollowersStalkersScreenType || screenType == LostFollowersStalkersScreenType
        || screenType == DontFollowBackStalkersScreenType || screenType == NotFollowMeBackStalkersScreenType || screenType == VisitorsStalkersScreenType) {
        
        _userNameTopConstraint.constant = 21;
    }
}

@end
