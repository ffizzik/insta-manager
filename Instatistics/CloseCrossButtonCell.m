//
//  CloseCrossButtonCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "CloseCrossButtonCell.h"


@implementation CloseCrossButtonCellSource

- (id)init {
  self = [super init];
  if (self) {
    self.cellClass = @"CloseCrossButtonCell";
      self.backgroundColor = UIColorFromRGBA(COLOR_DARK_PURPLE, 1.f);
      self.multipleSelection = YES;
  }
  return self;
}

@end


@implementation CloseCrossButtonCell

- (void)setUpWithSource:(CloseCrossButtonCellSource *)source {
    
    if (source.showRestoreButton)
        self.restoreButton.hidden = NO;
    else
        self.restoreButton.hidden = YES;
    
    [self.restoreButton setTintColor:[UIColor whiteColor]];
    self.backgroundColor = source.backgroundColor;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [_closeButton removeTarget:source.target action:source.selector forControlEvents:UIControlEventAllEvents];
    [_closeButton addTarget:source.target action:source.selector forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)restoreButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"restoreButtonPressedNotification" object:nil];
}

@end
