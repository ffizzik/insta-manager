//
//  DataLoader.h
//  InstaTracker
//
//  Created by Midnight.Works iMac on 10/24/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>





typedef void (^SuccessRH) (id responseObject);
typedef void (^FailRH) (NSError *error);




@interface DataLoader : NSObject



+(void)request:(NSString*)urlString parameters:(NSDictionary*)parameters success:(SuccessRH)success fail:(FailRH)fail;


@end
