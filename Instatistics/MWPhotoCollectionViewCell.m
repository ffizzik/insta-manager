//
//  MWPhotoCollectionViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWPhotoCollectionViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface MWPhotoCollectionViewCell()

@property (strong, nonatomic) UIImageView *gradientView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation MWPhotoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gradientView = [[UIImageView alloc] initWithFrame:self.bottomView.frame];
    self.gradientView.image = [CoreImageUtils imageFromVerticalGradientStartColor:[UIColor clearColor] EndColor: UIColorFromRGBA(COLOR_DARK_PURPLE, 0.7) Frame:self.gradientView.bounds];
    [self.bottomView insertSubview:self.gradientView atIndex:0];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.gradientView.frame = self.bottomView.bounds;
}

- (void)setupWithItem:(IGMedia *)media {

    _likesCountLabel.text = [AppUtils minimizedCountString:[media.likes_count integerValue]];
    _commentsCountLabel.text = [AppUtils minimizedCountString:[media.comment_count integerValue]];
    [_photoImageView sd_setImageWithURL:[NSURL URLWithString:media.image.standard_resolution]];
}

@end
