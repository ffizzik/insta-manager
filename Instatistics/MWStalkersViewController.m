//
//  MWStalkersViewController.m
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWStalkersViewController.h"
#import "MWStalkerTableViewCell.h"
#import "MWTextPlaceholderTableViewCell.h"
#import "MWProfileStalkerViewController.h"

#import "MainUser.h"
#import "UIImageView+AFNetworking.h"
#import "InstagramAPI.h"
#import "NSDate+NVTimeAgo.h"

static NSString *stalkerTableViewCellID = @"MWStalkerTableViewCell";
static NSString *textPlaceholderTableViewCellID = @"MWTextPlaceholderTableViewCell";

typedef NS_ENUM(NSUInteger, TableViewMode) {
    UsualMode = 0,
    SelectableMode
};

@interface MWStalkersViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *users;

@property (assign, nonatomic) TableViewMode tableViewMode;
@property (strong, nonatomic) NSMutableArray *followUsers;
@property (strong, nonatomic) NSMutableArray *getFollowingsResponseArray;

@property (strong, nonatomic) NSMutableArray *sectionTitles;
@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) NSMutableDictionary *sectionsDict;
@property (strong, nonatomic) NSArray *sortedSectionsArray;

@end

@implementation MWStalkersViewController {
    UIView *unfollowView;
    UIButton *unfollowButton;
}

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableViewMode = UsualMode;

    _users = [self stalkers];
    _sectionsDict = [NSMutableDictionary dictionary];
    
    for (int i = 0; i < _users.count; i++) {
        IGUser *selectedUser = [_users[i] objectForKey:@"user"];
        
        if (selectedUser.created_date) {
            NSString *key = [selectedUser.created_date formattedAsTimeAgo];
            
            if (_sectionsDict[key]) {
                [((NSMutableArray *)_sectionsDict[key][@"items"]) addObject:_users[i]];
            } else {
                NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:_users[i], nil];
                [_sectionsDict setObject:@{@"items" : array, @"date" : selectedUser.created_date} forKey:key];
            }
        } else {
            if (_sectionsDict[@" "]) {
                [((NSMutableArray *)_sectionsDict[@" "][@"items"]) addObject:_users[i]];
            } else {
                NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:_users[i], nil];
                [_sectionsDict setObject:@{@"items" : array, @"date" : @""} forKey:@" "];
            }
        }
    }
    
    BOOL ascendingOrder = NO;
    
    if (_stalkersScreenType == EarliestFollowersScreenType)
        ascendingOrder = YES;
    
    
    NSArray *sectionsArray = [[NSMutableArray alloc] initWithArray:[_sectionsDict allValues]];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                 ascending:ascendingOrder];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _sortedSectionsArray = [sectionsArray sortedArrayUsingDescriptors:sortDescriptors];

    _sectionTitles = [NSMutableArray array];
    
    for (int i = 0; i < _sortedSectionsArray.count; i++) {
        
        if ([[_sortedSectionsArray[i] objectForKey:@"date"] isKindOfClass:[NSString class]])
            [_sectionTitles addObject:@" "];
        else {
            NSDate *date = [_sortedSectionsArray[i] objectForKey:@"date"];
            if (date) {
                NSString *key = [date formattedAsTimeAgo];
                [_sectionTitles addObject:key];
            } else {
                [_sectionTitles addObject:@" "];
            }
        }
    }
    
    if (_sectionTitles.count == 1) {
        if ([_sectionTitles[0] isEqualToString:@" "])
            [self.tableView setContentInset:UIEdgeInsetsMake(-64, 0, 0, 0)];
    }
    
    self.tableView.allowsMultipleSelection = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:stalkerTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:stalkerTableViewCellID];
    [self.tableView registerNib:[UINib nibWithNibName:textPlaceholderTableViewCellID bundle:[NSBundle mainBundle]] forCellReuseIdentifier:textPlaceholderTableViewCellID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:_gothamPro_font_regular size:17]}];
    
    
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
    [self.view insertSubview:imageView atIndex:0];
    
    [self configureUI];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
    
    NSArray *indexPaths = self.tableView.indexPathsForSelectedRows;
    
    for (NSIndexPath *indexPath in indexPaths) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)configureUI {
    
    switch (_stalkersScreenType) {
        case VisitorsStalkersScreenType: {
            self.navigationItem.title = @"Profile Stalkers";
        } break;
            
        case NewFollowersStalkersScreenType: {
            self.navigationItem.title = @"Gained";
        } break;
            
        case LostFollowersStalkersScreenType: {
            self.navigationItem.title = @"Followers Lost";
        } break;
            
        case DontFollowBackStalkersScreenType: {
            self.navigationItem.title = @"I Don't Follow Back";
        } break;
            
        case NotFollowMeBackStalkersScreenType: {
            self.navigationItem.title = @"Not Following Me";
        } break;
            
        case BlockingMeStalkersScreenType: {
            self.navigationItem.title = @"Blocking Me";
        } break;
            
        case MostCommentsScreenType: {
            self.navigationItem.title = @"Most Comments to Me";
        } break;
            
        case MostCommentsAndLikesScreenType: {
            self.navigationItem.title = @"Most Comments and Likes";
        } break;
            
        case LeastLikesGivenScreenType: {
            self.navigationItem.title = @"Least Likes Given";
        } break;
            
        case LeastCommentsLeftScreenType: {
            self.navigationItem.title = @"Least Comments Left";
        } break;
            
        case NoCommentsOrLikesScreenType: {
            self.navigationItem.title = @"No Comments or Likes";
        } break;
            
        case MyRecentFavouriteUsersScreenType: {
            self.navigationItem.title = @"My Recent Favourite Users";
        } break;
            
        case MyBestFriendsScreenType: {
            self.navigationItem.title = @"My Best Friends";
        } break;
            
        case UsersILikeButDontFollowScreenType: {
            self.navigationItem.title = @"Users I Like But Don't Follow";
        } break;
            
        case MostLikesToMeScreenType: {
            self.navigationItem.title = @"Most Likes to Me";
        } break;
            
        case EarliestFollowersScreenType: {
            self.navigationItem.title = @"Earliest Followers";
        } break;
            
        case LatestFollowersScreenType: {
            self.navigationItem.title = @"Latest Followers";
        } break;
            
        case AllLostFollowersScreenType: {
            self.navigationItem.title = @"All Lost Followers";
        } break;
            
        case UsersIUnfollowedScreenType: {
            self.navigationItem.title = @"Users I Unfollowed";
        } break;
    }
    
    if (_tableViewMode == SelectableMode) {
        UIBarButtonItem *closeBI = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(onEditButtonPressed:)];
        self.navigationItem.rightBarButtonItem = closeBI;
    } else {
        UIBarButtonItem *closeBI = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStylePlain target:self action:@selector(onEditButtonPressed:)];
        self.navigationItem.rightBarButtonItem = closeBI;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (void)onEditButtonPressed:(id)sender {
    
    _followUsers = [NSMutableArray array];
    
    if (![unfollowView isDescendantOfView:self.view]) {
        
        CGRect frame = CGRectMake(0, self.view.frame.size.height - 64, self.view.frame.size.width, 64);
        
        unfollowView = [[UIView alloc] initWithFrame:frame];
        unfollowView.backgroundColor = [UIColor redColor];
        
        unfollowButton = [[UIButton alloc] initWithFrame:unfollowView.bounds];
        [unfollowButton.titleLabel setFont:[UIFont fontWithName:_gothamPro_font_medium size:14.f]];
        [unfollowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [unfollowButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        [unfollowButton addTarget:self action:@selector(onUnfollowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [unfollowView addSubview:unfollowButton];
        [self.view addSubview:unfollowView];
        
        unfollowView.transform = CGAffineTransformMakeTranslation(0, unfollowView.frame.origin.y + unfollowView.frame.size.height);
    }
    
    if (_tableViewMode == SelectableMode) {
        _tableViewMode = UsualMode;
        
        [UIView animateWithDuration:0.3 animations:^{
            unfollowView.transform = CGAffineTransformMakeTranslation(0, 2 * unfollowView.frame.size.height);
            
        } completion:^(BOOL finished) {
            unfollowView.hidden = YES;
        }];
        
    } else {
        _tableViewMode = SelectableMode;
        
        unfollowView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            unfollowView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {}];
    }
    
    [self.tableView reloadData];
    [self configureUI];
}

- (void)onUnfollowButtonPressed:(id)sender {
    
    NSMutableString *followUsersString = [NSMutableString string];
    
    for (int i = 0; i < _followUsers.count; i++) {
        IGUser *selectedUser = _followUsers[i];
        [followUsersString appendString:selectedUser.full_name];
        
        if (i < _followUsers.count - 1)
            [followUsersString appendString:@", "];
    }
            
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:followUsersString message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *button1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

    }];
    
    UIAlertAction *button2 = [UIAlertAction actionWithTitle:unfollowButton.titleLabel.text style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        __block NSInteger counter = 0;
        
        if (_followUsers.count > 0)
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        else {
            self.getFollowingsResponseArray = [NSMutableArray array];
            [self getFollowingsForId:@""];
            [alert dismissViewControllerAnimated:YES completion:nil];
        }
        
        for (int i = 0; i < _followUsers.count; i++) {
            IGUser *selectedUser = _followUsers[i];
            
            if ([unfollowButton.titleLabel.text isEqualToString:@"Unfollow"]) {
                [[InstagramAPI sharedInstance] unfollowUser:selectedUser.Id comp:^(int completition) {
                    counter++;
                    
                    if (counter == _followUsers.count) {
                        _followUsers = [NSMutableArray array];
                        _tableViewMode = UsualMode;
                        unfollowView.hidden = YES;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            MainUser *mainUser = [MainUser mainUser];
                            
                            [[mainUser.userFollowings realm] beginWriteTransaction];
                            [mainUser.userFollowings removeAllObjects];
                            [[mainUser.userFollowings realm] commitWriteTransaction];
                
                            self.getFollowingsResponseArray = [NSMutableArray array];
                            [self getFollowingsForId:@""];
                            
                            [alert dismissViewControllerAnimated:YES completion:nil];
                        });
                    }
                }];
                
            } else {
                [[InstagramAPI sharedInstance] followUser:selectedUser.Id comp:^(int completition) {
                    counter++;
                    
                    if (counter == _followUsers.count) {
                        _followUsers = [NSMutableArray array];
                        _tableViewMode = UsualMode;
                        unfollowView.hidden = YES;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            MainUser *mainUser = [MainUser mainUser];
                            
                            [[mainUser.userFollowings realm] beginWriteTransaction];
                            [mainUser.userFollowings removeAllObjects];
                            [[mainUser.userFollowings realm] commitWriteTransaction];
                            
                            self.getFollowingsResponseArray = [NSMutableArray array];
                            [self getFollowingsForId:@""];
                            
                            [alert dismissViewControllerAnimated:YES completion:nil];
                        });
                    }
                }];
            }
        }
    }];
    
    [alert addAction:button1];
    [alert addAction:button2];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)getFollowingsForId:(NSString *)nextId {
    
    MainUser *mainUser = [MainUser mainUser];
    
    [[InstagramAPI sharedInstance] getUserFollowing:mainUser.user.Id count:0 nextCursor:nextId comp:^(NSArray *response, IGPagination *pagination) {
        [self.getFollowingsResponseArray addObjectsFromArray:response];
        
        if ([[AppUtils validateString:pagination.nextMaxId] isEqualToString:@""] == NO) {
            [self getFollowingsForId:pagination.nextMaxId];
            
        } else {
            [self handleGetFollowingsResponse];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)handleGetFollowingsResponse {
    
    MainUser *mainUser = [MainUser mainUser];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    RLMThreadSafeReference *mainUserRef = [RLMThreadSafeReference referenceWithThreadConfined:mainUser];
    
    dispatch_async(queue, ^{
        RLMRealm *realm = [RLMRealm defaultRealm];
        MainUser *newMainUser = [realm resolveThreadSafeReference:mainUserRef];
        [MainUser addItems:self.getFollowingsResponseArray toArray:newMainUser.userFollowings];
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self.tableView reloadData];
            [self configureUI];
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    });
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_users.count == 0) return;
    
    NSArray *sectionUsers = [_sortedSectionsArray[indexPath.section] objectForKey:@"items"];
    NSDictionary *userDict = [sectionUsers objectAtIndex:indexPath.row];
    
    IGUser *selectedUser = [userDict objectForKey:@"user"];
    
    if (_tableViewMode == SelectableMode) {

        for (int i = 0; i < _followUsers.count; i++) {
            IGUser *user = _followUsers[i];
            if ([user.Id isEqualToString:selectedUser.Id]) {
                [_followUsers removeObject:selectedUser];
                
                IGUser *lastUser = [_followUsers.lastObject objectForKey:@"user"];
                if (lastUser) {
                    if ([self isFollowing:lastUser]) [unfollowButton setTitle:@"Unfollow" forState:UIControlStateNormal];
                    else                             [unfollowButton setTitle:@"Follow" forState:UIControlStateNormal];
                }
                
                return;
            }
        }
        
        [_followUsers addObject:selectedUser];
        
        IGUser *lastUser = _followUsers.lastObject;
        if (lastUser) {
            if ([self isFollowing:lastUser]) [unfollowButton setTitle:@"Unfollow" forState:UIControlStateNormal];
            else                             [unfollowButton setTitle:@"Follow" forState:UIControlStateNormal];
        }
        
        return;
    }

    MWProfileStalkerViewController *vc = [MWProfileStalkerViewController new];
    vc.user = selectedUser;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_users.count == 0) return;
    
    if (_tableViewMode == SelectableMode) {
        
        NSArray *sectionUsers = [_sortedSectionsArray[indexPath.section] objectForKey:@"items"];
        NSDictionary *userDict = [sectionUsers objectAtIndex:indexPath.row];
        
        IGUser *selectedUser = [userDict objectForKey:@"user"];
        
        for (int i = 0; i < _followUsers.count; i++) {
            IGUser *user = _followUsers[i];
            if ([user.Id isEqualToString:selectedUser.Id]) {
                [_followUsers removeObject:selectedUser];
                
                IGUser *lastUser = _followUsers.lastObject;
                if (lastUser) {
                    if ([self isFollowing:lastUser]) [unfollowButton setTitle:@"Unfollow" forState:UIControlStateNormal];
                    else                             [unfollowButton setTitle:@"Follow" forState:UIControlStateNormal];
                }
                
                return;
            }
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_users.count == 0) return self.view.frame.size.height - 20;
    else                   return 95;
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 64)];

    if (_sectionTitles.count > 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width - 8, 64)];
        label.textAlignment = NSTextAlignmentRight;
        [label setTextColor:[UIColor whiteColor]];
        [label setFont:[UIFont fontWithName:_gothamPro_font_light size:16.f]];
        
        
        NSString *string = [_sectionTitles objectAtIndex:section];
        [label setText:string];
        [view addSubview:label];
        [view setBackgroundColor:[UIColor clearColor]];
        
        if (_sectionTitles.count == 1) {
            if ([_sectionTitles[0] isEqualToString:@" "]){
                
            } else {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:view.frame];
                imageView.image = [CoreImageUtils imageFromVerticalGradientStartColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
                [view insertSubview:imageView atIndex:0];
            }
        } else {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:view.frame];
            imageView.image = [CoreImageUtils imageFromVerticalGradientStartColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_DARK_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_DARK_PURPLE, 1.f) Frame:imageView.bounds];
            [view insertSubview:imageView atIndex:0];
        }
        
    } else {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_sectionTitles.count > 0) {
        return 64;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_sectionTitles.count == 0) return 1;
    return _sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_users.count == 0) return 1;
    else {
        return ((NSArray *)[_sortedSectionsArray[section] objectForKey:@"items"]).count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_users.count == 0) {
        MWTextPlaceholderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:textPlaceholderTableViewCellID];
        return cell;
    }
    
    MWStalkerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:stalkerTableViewCellID];
    
    [cell configureWithStalkersScreenType:_stalkersScreenType];
    
    NSArray *sectionUsers = [_sortedSectionsArray[indexPath.section] objectForKey:@"items"];
    NSDictionary *userDict = [sectionUsers objectAtIndex:indexPath.row];
    
    if (_stalkersScreenType == MostCommentsScreenType || _stalkersScreenType == LeastCommentsLeftScreenType) {
        if ([userDict objectForKey:@"commentCount"]) {
            NSInteger count = [[userDict objectForKey:@"commentCount"] integerValue];
            if (count == 1) cell.commentsLabel.text = [NSString stringWithFormat:@"%ld comment", count];
            else            cell.commentsLabel.text = [NSString stringWithFormat:@"%ld comments", count];
        }
        
    } else if (_stalkersScreenType == MostCommentsAndLikesScreenType) {
        if ([userDict objectForKey:@"mediaCount"]) {
            NSInteger count = [[userDict objectForKey:@"mediaCount"] integerValue];
            if (count == 1) cell.commentsLabel.text = [NSString stringWithFormat:@"%ld comment or like", count];
            else            cell.commentsLabel.text = [NSString stringWithFormat:@"%ld comments and likes", count];
        }
        
    } else if (_stalkersScreenType == LeastLikesGivenScreenType || _stalkersScreenType == MostLikesToMeScreenType) {
        if ([userDict objectForKey:@"likesCount"]) {
            NSInteger count = [[userDict objectForKey:@"likesCount"] integerValue];
            if (count == 1) cell.commentsLabel.text = [NSString stringWithFormat:@"%ld like", count];
            else            cell.commentsLabel.text = [NSString stringWithFormat:@"%ld likes", count];
        }
    }
    
    if (_tableViewMode == UsualMode) {
        
        cell.selectedViewLeadingConstraint.constant = -24;
            
        [UIView animateWithDuration:0.3 animations:^{
            cell.selectedView.alpha = 0.0f;
            [cell layoutIfNeeded];
        } completion:^(BOOL finished) {
            cell.selectedView.hidden = YES;
        }];
        
    } else {
        
        if (cell.selectedViewLeadingConstraint.constant != 0) {
            cell.selectedView.hidden = NO;
            cell.selectedViewLeadingConstraint.constant = 0;
            
            [UIView animateWithDuration:0.3 animations:^{
                cell.selectedView.alpha = 1.0f;
                [cell layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
    }
    
    /* ------------------------------------- */
    
    MainUser *mainUser = [MainUser mainUser];
    IGUser *user = [userDict objectForKey:@"user"];
    
    BOOL isFollower = NO;
    
    for (IGUser *follower in mainUser.userFollowers)
        if ([follower.Id isEqualToString:user.Id])
            isFollower = YES;
    
    if (isFollower) cell.followsYouView.hidden = NO;
    else            cell.followsYouView.hidden = YES;
    
    BOOL isFollowing = NO;
    
    for (IGUser *following in mainUser.userFollowings)
        if ([following.Id isEqualToString:user.Id])
            isFollowing = YES;
    
    if (isFollowing) cell.youFollowView.hidden = NO;
    else             cell.youFollowView.hidden = YES;
    
    if (isFollower && !isFollowing) {
        cell.followsYouTopConstraint.constant = 25;
    } else if (!isFollower && isFollowing) {
        cell.youFollowBottomConstraint.constant = 25;
    } else {
        cell.youFollowBottomConstraint.constant = 9;
        cell.followsYouTopConstraint.constant = 9;
    }
    
    [cell.userImageView setImageWithURL:[NSURL URLWithString:user.profile_picture]];
    cell.userNameLabel.text = user.full_name;
    cell.userNickNameLabel.text = user.username;
    
    //NSLog(@"%@", [user.created_date formattedAsTimeAgo]);
    
    return cell;
}

#pragma mark - Helpers

- (BOOL)isFollowing:(IGUser *)user {
    
    MainUser *mainUser = [MainUser mainUser];
    
    BOOL isFollowing = NO;
    
    for (IGUser *following in mainUser.userFollowings)
        if ([following.Id isEqualToString:user.Id])
            isFollowing = YES;

    return isFollowing;
}

#pragma mark - Database Calculations

- (NSMutableArray *)stalkers {
    
    MainUser *mainUser = [MainUser mainUser];
    
    switch (_stalkersScreenType) {
        case VisitorsStalkersScreenType: {
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *liker in likes)
                dict[liker.user.Id] = @{@"user" : liker.user};
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"user.created_date"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case NewFollowersStalkersScreenType: {
            
            RLMArray *users = mainUser.userNewFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users)
                dict[user.Id] = @{@"user" : user};
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"user.created_date"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case LostFollowersStalkersScreenType: {
            
            RLMArray *users = mainUser.userLostFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users)
                dict[user.Id] = @{@"user" : user};
            
            return [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
        } break;
            
        case DontFollowBackStalkersScreenType: {
            
            RLMArray *users = mainUser.userIAmNotFollowingBack;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users)
                dict[user.Id] = @{@"user" : user};
            
            return [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
        } break;
            
        case NotFollowMeBackStalkersScreenType: {
            
            RLMArray *users = mainUser.usersNotFollowMeBack;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users)
                dict[user.Id] = @{@"user" : user};
            
            return [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
        } break;

        case BlockingMeStalkersScreenType: {
            
            RLMArray *users;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users)
                dict[user.Id] = @{@"user" : user};
            
            return [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
        } break;
        
        case MostCommentsScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *comments = mainUser.comments;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGComment *comment in comments) {
                
                if ([comment.user.Id isEqualToString:mainUser.user.Id])
                    continue;
                
                if ([dict objectForKey:comment.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:comment.user.Id][@"commentCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[comment.user.Id] = @{@"user" : comment.user, @"commentCount" : currentCommentsCount};
                                                      
                } else {
                    dict[comment.user.Id] = @{@"user" : comment.user, @"commentCount" : @(1)};
                }
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"commentCount"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case MostCommentsAndLikesScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *comments = mainUser.comments;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGComment *comment in comments) {
                if ([comment.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                if ([dict objectForKey:comment.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:comment.user.Id][@"mediaCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[comment.user.Id] = @{@"user" : comment.user, @"mediaCount" : currentCommentsCount};
                    
                } else
                    dict[comment.user.Id] = @{@"user" : comment.user, @"mediaCount" : @(1)};
            }
            
            RLMArray *likes = mainUser.likes;
            
            for (IGLiker *like in likes) {
                if ([like.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                if ([dict objectForKey:like.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:like.user.Id][@"mediaCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[like.user.Id] = @{@"user" : like.user, @"mediaCount" : currentCommentsCount};
                    
                } else
                    dict[like.user.Id] = @{@"user" : like.user, @"mediaCount" : @(1)};
            }
            
            /* ------------------------------ */
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mediaCount"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case LeastLikesGivenScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *like in likes) {
                if ([like.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                if ([dict objectForKey:like.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:like.user.Id][@"likesCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : currentCommentsCount};
                    
                } else
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : @(1)};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"likesCount"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case LeastCommentsLeftScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *comments = mainUser.comments;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGComment *comment in comments) {
                if ([comment.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                if ([dict objectForKey:comment.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:comment.user.Id][@"commentCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[comment.user.Id] = @{@"user" : comment.user, @"commentCount" : currentCommentsCount};
                    
                } else
                    dict[comment.user.Id] = @{@"user" : comment.user, @"commentCount" : @(1)};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"commentCount"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case NoCommentsOrLikesScreenType: {
            
            RLMArray *users = mainUser.userNewFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            RLMArray *likes = mainUser.likes;
            
            for (IGUser *user in users) {
                
                bool liked = false;
                
                for (IGLiker *liker in likes) {
                    if ([liker.user.Id isEqualToString:user.Id]) {
                        liked = true;
                        break;
                    }
                }
                
                if (!liked)
                    dict[user.Id] = @{@"user" : user};
            }
            
            return [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
        } break;
            
        case MyRecentFavouriteUsersScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *like in likes) {
                if ([like.user.Id isEqualToString:mainUser.user.Id]) continue;
            
                long dateTimeInterval = (long)[[NSDate date] timeIntervalSince1970];
                
                if ([like.created_date timeIntervalSince1970] + (7 * 24 * 60 * 60) <= dateTimeInterval) { // (7 * 24 * 60 * 60) - week
                    continue;
                }
      
                if ([dict objectForKey:like.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:like.user.Id][@"likesCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : currentCommentsCount};
                    
                } else
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : @(1)};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"likesCount"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case MyBestFriendsScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *like in likes) {
                if ([like.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                long dateTimeInterval = (long)[[NSDate date] timeIntervalSince1970];
                
                if ([like.created_date timeIntervalSince1970] + (30 * 7 * 24 * 60 * 60) <= dateTimeInterval) { // (30 * 7 * 24 * 60 * 60) - month
                    continue;
                }
      
                if ([dict objectForKey:like.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:like.user.Id][@"likesCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : currentCommentsCount};
                    
                } else
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : @(1)};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"likesCount"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case UsersILikeButDontFollowScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *like in likes) {
                if (![like.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                dict[like.user.Id] = @{@"user" : like.user};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            return [[NSMutableArray alloc] initWithArray:users];
            
        } break;
            
        case MostLikesToMeScreenType: {
            
            MainUser *mainUser = [MainUser mainUser];
            
            RLMArray *likes = mainUser.likes;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGLiker *like in likes) {
                if ([like.user.Id isEqualToString:mainUser.user.Id]) continue;
                
                if ([dict objectForKey:like.user.Id]) {
                    NSNumber *currentCommentsCount = [dict objectForKey:like.user.Id][@"likesCount"];
                    currentCommentsCount = [NSNumber numberWithInt:[currentCommentsCount intValue] + 1];
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : currentCommentsCount};
                    
                } else
                    dict[like.user.Id] = @{@"user" : like.user, @"likesCount" : @(1)};
            }
            
            NSArray *users = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"likesCount"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [users sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case EarliestFollowersScreenType: {
            
            MainUser *storedUser = [MainUser storedUser];
            
            RLMArray *users = storedUser.userFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users) {
                if (user.created_date)
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @([user.created_date timeIntervalSinceNow])};
                else
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @(0)};
            }
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate"
                                                         ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case LatestFollowersScreenType: {
            
            MainUser *storedUser = [MainUser storedUser];
            
            RLMArray *users = storedUser.userFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users) {
                if (user.created_date)
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @([user.created_date timeIntervalSinceNow])};
                else
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @(0)};
            }
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case AllLostFollowersScreenType: {
            
            MainUser *storedUser = [MainUser storedUser];
            
            RLMArray *users = storedUser.userLostFollowers;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users) {
                if (user.created_date)
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @([user.created_date timeIntervalSinceNow])};
                else
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @(0)};
            }
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
            
        case UsersIUnfollowedScreenType: {
            
            MainUser *storedUser = [MainUser storedUser];
            
            RLMArray *users = storedUser.userIAmNotFollowingBack;
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            for (IGUser *user in users) {
                if (user.created_date)
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @([user.created_date timeIntervalSinceNow])};
                else
                    dict[user.Id] = @{@"user" : user, @"createdDate" : @(0)};
            }
            
            NSArray *followers = [[NSMutableArray alloc] initWithArray:[dict allValues]];
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [followers sortedArrayUsingDescriptors:sortDescriptors];
            
            return [[NSMutableArray alloc] initWithArray:sortedArray];
            
        } break;
    }
}

@end
