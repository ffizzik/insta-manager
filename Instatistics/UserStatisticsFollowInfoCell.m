//
//  UserStatisticsFollowInfoCell.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "UserStatisticsFollowInfoCell.h"
#import "AppUtils.h"
#import "FollowMenuCell.h"
#import "AppConstants.h"
#import "VerticalMenuItem.h"
#import "MainUser.h"
#import "InstagramAPI.h"
#import "RegisteredUser.h"


@implementation UserStatisticsFollowInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    [_collectionView registerNib:[UINib nibWithNibName:@"FollowMenuCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"FollowMenuCell"];
    [_collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = collectionView.frame.size.width / 2.0f;
    return CGSizeMake(width, 0.7 * width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return _itemsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FollowMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FollowMenuCell" forIndexPath:indexPath];
    cell.index = indexPath.row;
    cell.target = self.target;
    [cell setupWithItem:_itemsArray[indexPath.row]];
    return cell;
}

@end
