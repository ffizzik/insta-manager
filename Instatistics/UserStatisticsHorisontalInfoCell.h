//
//  UserStatisticsHorisontalInfoCell.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//


#import "IGUser.h"
#import "MainUser.h"

@interface UserStatisticsHorisontalInfoCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *itemsArray;

@property (nonatomic, assign) BOOL isTracking;
@property (nonatomic, assign) BOOL isRunning;

@end

