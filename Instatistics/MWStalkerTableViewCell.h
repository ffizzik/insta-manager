//
//  MWStalkerTableViewCell.h
//  Instatistics
//
//  Created by Denis Svichkarev on 27/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWStalkersViewController.h"

@interface MWStalkerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;

@property (weak, nonatomic) IBOutlet UIView *followingView;
@property (weak, nonatomic) IBOutlet UIView *followsYouView;
@property (weak, nonatomic) IBOutlet UIView *youFollowView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followsYouTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youFollowBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followingViewWidthConstraint;

@property (weak, nonatomic) IBOutlet UIView *selectedView;
@property (weak, nonatomic) IBOutlet UIView *selectedInnerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedViewLeadingConstraint; // -34 or 0
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNameTopConstraint; // 21 or 13

- (void)configureWithStalkersScreenType:(MWStalkersScreenType)screenType;

@end
