//
//  MWDeletedLikesViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 02/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MWDeletedContentScreenType) {
    MWDeletedLikesContentScreenType = 0,
    MWDeletedPhotosContentScreenType,
};

@interface MWDeletedLikesViewController : UIViewController

@property (assign, nonatomic) MWDeletedContentScreenType screenType;
    
@end
