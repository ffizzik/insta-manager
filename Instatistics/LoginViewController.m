//
//  LoginViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "CustomInputView.h"
#import "BaseNavigationController.h"
#import "AccountStateViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>


@interface LoginViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *loginWebview;
@property (strong, nonatomic) IBOutlet UITextField *loginTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationBarHeightConstraint;

@end

@implementation LoginViewController

#pragma mark - Controller Life Cycle

- (IBAction)loginAction:(id)sender
{
    [SVProgressHUD show];

    [[InstagramAPI sharedInstance] loginWithUsername:self.loginTF.text andPassword:self.passwordTF.text comp:^(BOOL response) {
        [SVProgressHUD dismiss];

        if (!response) {
            [SVProgressHUD showErrorWithStatus:@"Incorrect login info"];
        } else {
            [self checkSubscriptions];
        }
        
    } failure:^(NSError * error) {
        
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setBackgroundLayerColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.9]];

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && UIScreen.mainScreen.nativeBounds.size.height == 2436)  {
        _navigationBarHeightConstraint.constant = 88.0;
        [self.view layoutIfNeeded];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    CGRect bounds = self.topView.bounds;
    gradient.frame = bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:64.0/255.0 green:106.0/255.0 blue:149.0/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:38.0/255.0 green:77.0/255.0 blue:121.0/255.0 alpha:1.0] CGColor], nil];
    
    [self.topView.layer insertSublayer:gradient atIndex:0];
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    CGRect bounds1 = self.loginButton.bounds;
    
    gradient1.frame = bounds1;
    gradient1.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:87.0/255.0 green:165.0/255.0 blue:117.0/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:47.0/255.0 green:124.0/255.0 blue:75.0/255.0 alpha:1.0] CGColor], nil];
    
    [self.loginButton.layer insertSublayer:gradient1 atIndex:0];
    self.loginButton.layer.masksToBounds = YES;
    self.loginButton.layer.cornerRadius = 5.0;


    UIImage *img = [[UIImage imageNamed:@"logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.logoView.image = img;
    self.logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.logoView.backgroundColor = [UIColor clearColor];
}

- (UIImage *)imageFromLayer:(CALayer *)layer
{
    CGRect rect = CGRectMake([layer frame].origin.x, [layer frame].origin.y, [layer frame].size.width, [layer frame].size.height-43.0);

    UIGraphicsBeginImageContext(rect.size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];
}

#pragma mark - Actions

- (IBAction)login:(UIButton*)sender {

}

- (IBAction)closeView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self.navigationController setNavigationBarHidden:[AppUtils isIphoneVersion:4] || [AppUtils isIphoneVersion:5]  animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //[super keyboardWillHide:notification];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {}

- (void)webViewDidStartLoad:(UIWebView *)webView {}

- (void)webViewDidFinishLoad:(UIWebView *)webView {}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.absoluteString containsString:[NSString stringWithFormat:@"%@config2.php", MAINLINK]]) {
        
        NSArray *credentials = [request.URL.absoluteString componentsSeparatedByString:@"username="];
        
        if (credentials.count == 2) {
            NSArray * credentials2 = [credentials[1] componentsSeparatedByString:@"&password="];
            if (credentials2.count == 2) {
                NSString * username = credentials2[0];
                NSString * password = credentials2[1];
                
                if(username.length > 0 && password.length > 0) {
                    
                    [[InstagramAPI sharedInstance] loginWithUsername:username andPassword:password comp:^(BOOL response) {
                        
                        if (!response) {
                            NSString *urlAddress = [NSString stringWithFormat:@"%@failConfig.php?username=%@", MAINLINK, username];
                                                   
                            NSURL *url = [NSURL URLWithString:urlAddress];
                            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
                            [_loginWebview loadRequest:requestObj];
                            
                        } else {
                            [self checkSubscriptions];
                        }
                        
                    } failure:^(NSError * error) {
                        
                    }];
                    
                } else {
                    NSString *urlAddress = [NSString stringWithFormat:@"%@failConfig.php?username=", MAINLINK];
                    NSURL *url = [NSURL URLWithString:urlAddress];
                    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
                    [_loginWebview loadRequest:requestObj];
                }
            }
        }
        
        return NO;
    }
    
    /*if ([request.URL.absoluteString containsString:@"http://insta.com/"]) {
        NSArray * credentials = [request.URL.absoluteString componentsSeparatedByString:@"username="];
        
        if (credentials.count == 2) {
            NSArray * credentials2 = [credentials[1] componentsSeparatedByString:@"&password="];
            if (credentials2.count == 2) {
                NSString * username = credentials2[0];
                NSString * password = credentials2[1];
                
                [[InstagramAPI sharedInstance] loginWithUsername:username andPassword:password comp:^(BOOL response) {
                    
                    if (response) {
                        [self checkSubscriptions];
                    }
                    
                } failure:^(NSError * error) {}];
                return NO;
            }
        }
    }*/

    return YES;
}

- (void)checkSubscriptions {
    
    [[NetworkFetcher sharedFetcher] checkInAppPurchaseStatusCompletion:^(BOOL result) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (result) {
            NSLog(@"Verified");
            [Storage setSubsciptionsStatus:YES];
            
        } else {
            NSLog(@"Not verified");
            [Storage setSubsciptionsStatus:NO];
        }
        
        BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
        StartPageViewController *startPage = navigationController.startViewController;
        startPage.mainUser = [[InstagramAPI sharedInstance] loggedInUser];
        
        [self performSegueWithIdentifier:segue_showStatistics sender:nil];
    }];
}

@end
