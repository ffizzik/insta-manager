//
//  UITableView+MWHelpers.m
//  
//
//  Created by Denis Svichkarev on 04.03.16.
//
//

#import "UITableView+MWHelpers.h"
#import "MWKeyboard.h"

@implementation UITableView (MWHelpers)

- (void)adjustInsetWithBottomHeight:(CGFloat)bottomHeight {
    [UIView animateWithDuration:[MWKeyboard sharedInstance].animationSpeed animations:^{
        
        CGFloat height = [MWKeyboard sharedInstance].isVisible ? [MWKeyboard sharedInstance].height : bottomHeight;
        
        UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, height, 0);
        self.contentInset = adjustForTabbarInsets;
        self.scrollIndicatorInsets = adjustForTabbarInsets;
    }];
}

@end
