//
//  AccountStateViewController.m
//  Instatistics
//
//  Created by 1 on 07/02/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "AccountStateViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "StartPageViewController.h"

#import "UITableView+MWHelpers.h"
#import "InstagramAPI.h"

@interface AccountStateViewController()

@property (weak, nonatomic) IBOutlet UIButton *connectButton;

@end

@implementation AccountStateViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem new] initWithCustomView:[UIButton buttonWithType:UIButtonTypeCustom]];
    self.navigationController.navigationBarHidden = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Action

- (IBAction)onSignInButtonPressed:(id)sender {
    [self performSegueWithIdentifier:segue_showLoginView sender:nil];
}

- (void)showErrorAlertWithTitleMessage:(NSString *)titleMessage
                               Message:(NSString *)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleMessage message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *button1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:button1];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:segue_showLoginView]) {
        LoginViewController *login = segue.destinationViewController;
        login.target = self;
    }
}

@end
