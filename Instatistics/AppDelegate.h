//
//  AppDelegate.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 11/28/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (UIViewController *)topMostController;

@end

