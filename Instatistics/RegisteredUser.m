//
//  RegisteredUser.m
//  DeepFollowers Tracker
//
//  Created by Midnight.Works iMac on 12/13/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "RegisteredUser.h"


@implementation RegisteredUser

+ (RLMResults *)allUsers {
    return [RegisteredUser allObjects];
}

@end
