//
//  NetworkFetcher.h
//  Instatistics
//
//  Created by . on 28/03/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkFetcher : NSObject

+ (instancetype)sharedFetcher;

- (void)checkInAppPurchaseStatusCompletion:(void (^)(BOOL result))completion;

- (void)loginWithEmail:(NSString *)email Password:(NSString *)password Completion:(void (^)(BOOL result))completion;

@end
