//
//  RegisteredUser.h
//  DeepFollowers Tracker
//
//  Created by Midnight.Works iMac on 12/13/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import <Realm/Realm.h>
#import "MainUser.h"

@interface RegisteredUser : RLMObject

@property MainUser *firstRegisteredUser;
@property MainUser *currentUser;
@property MainUser *previousUser;
@property MainUser *storedUser;

@property NSString *userId;

+ (RLMResults *)allUsers;

@end
