//
//  MWProfileStalkerViewController.h
//  Instatistics
//
//  Created by Denis Svichkarev on 30/04/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainUser.h"

@interface MWProfileStalkerViewController : UIViewController

@property (strong, nonatomic) IGUser *user;

@end
