//
//  StartPageViewController.m
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "StartPageViewController.h"
#import "MyPostsCell.h"
#import "BaseNavigationController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AccountStateViewController.h"

#import "Storage.h"

@interface StartPageViewController ()

@end

@implementation StartPageViewController

#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setBackgroundLayerColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.9]];
    
    self.mainUser = [MainUser mainUser];
    
    BaseNavigationController *navControll = (BaseNavigationController *)self.navigationController;
    navControll.startViewController = self;
    
    [self checkSubscriptions];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([SVProgressHUD isVisible])
        [SVProgressHUD dismiss];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (void)checkSubscriptions {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
  
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    activityView.center = window.center;
    [activityView startAnimating];
    [window addSubview:activityView];
    
    [[NetworkFetcher sharedFetcher] checkInAppPurchaseStatusCompletion:^(BOOL result) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [activityView stopAnimating];
        [activityView removeFromSuperview];
        
        if (result) {
            NSLog(@"Verified");
            [Storage setSubsciptionsStatus:YES];
            
        } else {
            NSLog(@"Not verified");
            [Storage setSubsciptionsStatus:NO];
        }
        
        NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
        
        if ([AppUtils isValidObject:self.mainUser.user] && token) {
            [self performSegueWithIdentifier:segue_showStatistics sender:nil];
        }
        else {
            [self performSegueWithIdentifier:segue_showAccountState sender:@(_not_connected)];
        }
    }];
}

- (void)leftMenuItemSelected:(UIButton *)sender {
  
    if (sender.tag < 4) {
        if (self.selectedIndex == sender.tag) {
            return;
        } else {
            [self.navigationController popToViewController:self animated:NO];
            self.selectedIndex = sender.tag;
        }
    }
    
    switch (sender.tag) {
        case 0: {
            //[SVProgressHUD show];
            [self performSegueWithIdentifier:segue_showStatistics sender:nil];
        } break;
            
        case 1: {
            [self performSegueWithIdentifier:segue_showMyPosts sender:nil];
        } break;
            
        case 2: {
            [self performSegueWithIdentifier:segue_showAudience sender:nil];
        } break;
            
        case 3: {
            [self performSegueWithIdentifier:segue_showEngagement sender:nil];
        } break;
            
        case 4: {
            [AppUtils openUrl:@"http://midnight.works"];
        } break;
    
        default: break;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:segue_showAccountState]) {
        
        //AccountStateViewController *stateViewController = segue.destinationViewController;
        //stateViewController.connectionState = [sender intValue];
    }
}

@end
