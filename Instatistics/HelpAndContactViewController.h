//
//  HelpAndContactViewController.h
//  Instatistics
//
//  Created by Midnight.Works iMac on 10/4/16.
//  Copyright © 2016 TUSK.ONE. All rights reserved.
//

#import "BaseViewController.h"

@interface HelpAndContactViewController : BaseViewController

@property (nonatomic, assign) RequestName requestName;

@end
