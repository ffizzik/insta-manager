//
//  MWGradientHeaderTableViewCell.m
//  Instatistics
//
//  Created by Denis Svichkarev on 06/05/2017.
//  Copyright © 2017 TUSK.ONE. All rights reserved.
//

#import "MWGradientHeaderTableViewCell.h"

@interface MWGradientHeaderTableViewCell()

@property (strong, nonatomic) UIImageView *gradientView;

@end

@implementation MWGradientHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gradientView = [[UIImageView alloc] initWithFrame:self.frame];
    self.gradientView.image = [CoreImageUtils imageFromGradientStartColor:UIColorFromRGBA(COLOR_LEFT_GRADIENT_PURPLE, 1.f) EndColor:UIColorFromRGBA(COLOR_RIGHT_GRADIENT_PURPLE, 1.f) Frame:self.gradientView.bounds];
    [self insertSubview:self.gradientView atIndex:0];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.gradientView.frame = self.bounds;
}

- (void)configureWithIcon:(UIImage *)icon Title:(NSString *)title {

    self.iconImageView.image = icon;
    self.mainTitleLabel.text = title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
